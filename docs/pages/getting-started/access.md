# How to access your Crafty instance :material-cursor-default-click:
Crafty is a web-based application, which means you'll need a web browser to access and use it. This page provides useful tips and instructions on how to get up and running smoothly with Crafty.
## Use HTTPS, not HTTP :material-hand-back-left:

Crafty makes use of [HTTPS][https example]{:target="_blank"} to protect your personal data. When accessing Crafty you must explicitly type https when entering Crafty’s address in your browser. i.e. [https://127.0.0.1:8443](https://127.0.0.1:8443){:target="_blank"}

Crafty can be accessed via `localhost`, `127.0.0.1`, and `your machine’s IP address`,<br> but again, you must type **https://** before the address.

!!! note "Crafty's self-signed certificate"
    You may recieve a warning screen in your browser notifying "Your connection is not private", don't worry it is, it's just because crafty by default uses a self-signed cert, which your browser can't verify against the big certificate authorities.

    Just click the dropdown and proceed anyway!

[https example]: https://www.cloudflare.com/learning/ssl/what-is-https "hypertext transfer protocol secure"

## Logging in :fontawesome-solid-person-through-window:
Once you reach the Crafty webpage you will be greeted with a login screen.

The default credentials in `default.json` are:<br>
Username: **admin**<br>
Password: **crafty**

**We highly recommend changing this immediately after logging in.**
![NPM Add Host example](../../img/page-assets/getting-started/access/crafty-4-login.png)

## Welcome to Crafty
![Crafty Dashboard](../../img/page-assets/getting-started/access/crafty-4-dashboard.png)

Once you are logged in you will see this welcome screen. **This is your dashboard.** From here you can access settings pages and create servers. When you have servers created, you will see them appear here.

-----

<center>
So you ready to create some servers? I know I am, click the button below!
[Make your first Server :rocket:](../user-guide/server-creation/minecraft.md){ .md-button .md-button--primary }
</center>
