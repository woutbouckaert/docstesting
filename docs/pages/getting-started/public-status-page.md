# The Public Status Page :material-list-status:

![Public status overview](../../img/page-assets/getting-started/public-status-page/public-status-page.png)

The public status page, accessible at the `/status`<small>:material-information-outline:{ title="For example https://localhost:8443/status" }</small> route, offers users a convenient way to stay informed about the server's current status, player count, and message of the day (MOTD).

This often overlooked feature provides transparency and real-time updates on whether the server is online or offline, allowing players to quickly determine its availability.

Additionally, the status page displays the current player count, offering insights into server activity.

<center>
## Exempting Servers from the Status Page
If you'd prefer certain servers to not be displayed on the public status page, all you need to do is turn off the toggle on the servers configuration page.

![Public status exempt exxample](../../img/page-assets/getting-started/public-status-page/public-status-page-config.png)
</center>
