# Contributing
<center>
Welcome to Crafty's contributing community! 🚀

We greatly appreciate your involvement in improving Crafty and making it even better for everyone. There are several ways you can contribute and help us enhance the overall experience.

By actively participating in these areas, you contribute to the growth and improvement of Crafty, benefiting the entire community.

Thank you for your contributions and for being a part of our journey!

Together, we can make Crafty the best it can be! ✨
</center>

### Creating an issue

!!! bug ""
    :material-bug: __Something is not working?__

    ---

    Report a bug in Crafty Controller by creating an issue and a reproduction

    [:octicons-arrow-right-24: Report a bug][report a bug]

!!! abstract ""
    :material-file-document: __Missing information in our docs?__

    ---

    Report missing information or potential inconsistencies in our documentation

    [:octicons-arrow-right-24: Report a docs issue][report a docs issue]

!!! example ""
    :material-lightbulb-on: __Want to submit an idea?__

    ---

    Propose a change or feature request or suggest an improvement

    [:octicons-arrow-right-24: Request a change][request a change]

!!! question ""
    :material-chat-question: __Have a question or need help?__

    ---

    Ask questions on our Discord and get in touch with our community

    [:octicons-arrow-right-24: Ask a question][ask a question]


  [report a bug]: bug-reporting.md
  [report a docs issue]: doc-issue.md
  [request a change]: change-requests.md
  [ask a question]: ask-a-question.md
