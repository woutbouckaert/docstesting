# Minecraft Server Creation :material-minecraft:

Crafty 4 allows you to choose Minecraft Java or Bedrock.
Select this by choosing Minecraft-Bedrock or Minecraft-Java on the top navigation once you are in server creation.

## Create a new server
This is a server creation tool. It gives you a selection of options for customising your dedicated server installation. Set the options you require, Crafty will download and prepare your Minecraft server.

Near the bottom you will designate your min/max ram and designate a port. You can also choose to add the server to an existing role using the dropdown.
??? note "Server Port - Docker Port Range"
    Remember, with docker you will need to match the ports that you've made available to your container. If you are using the [provided examples](../../getting-started/installation/docker.md#installation), you should have 100 ports to play with.
![Server Builder Panel](../../../img/page-assets/user-guide/server-creation/minecraft/server-builder.png)

<center><small>*Big thank you to [ServerJars](https://serverjars.com){:target="_blank"} for providing the API that serves jar content to this panel.* 💖</small></center>

## Import a server
If you already have a Minecraft server that is already set up, you can import this into Crafty!

Just bare in mind...

When you import a server, we copy the server data into **internal crafty managed directory**.<br> This means that you will need to have double the amount of storage available when importing servers. After the import, you can remove your old copy of your server or keep it as a backup.

=== "Directory Import <sub>Local Storage</sub>"
    <center>This option is to import a server that already exists on your harddrive.</center>
    !!! example ""
        ![Directory import example](../../../img/page-assets/user-guide/server-creation/minecraft/server-dir-import.png){ align=left }

        Here you must provide a few bits of information:<br>
        - The path the files currently live in.<br>
        - The name of the executable file in that server directory.

=== "Zip Import <sub>Local Storage</sub>"
    <center>This option is to import a server that is packaged in a zip file on your harddrive.</center>
    !!! example ""
        Very similar to the directory import but we just need the path where the .zip file is located.

        Once entered, click on <sub><sub>![Root dir select example](../../../img/page-assets/user-guide/server-creation/minecraft/zip-import-root-dir-select.png)</sub></sub>

        ![root dir select example](../../../img/page-assets/user-guide/server-creation/minecraft/rood-dir-select-example.png)

        You will be directed to a selection screen. Here, you will select the directory you want as your root directory for your server. The root directory in this archive is the one base directory where all the server files are stored so I’ll just keep “Files” checked off.

=== "Zip Import <sub>Panel Upload</sub>"
    <center>This option is to upload a zip file containing the server files to the dashboard.</center>
    !!! example ""
        Very similar to the local zip import, instead we're uploading the zip to the dashboard.

        Choose the zip containing your server files and click upload.

        ⚠️ NOTE: If you are using [Cloudflare Free/Pro](https://www.cloudflare.com/en-gb/plans/){:target="_blank"}, your `Client Max Upload Size` is restricted to `100MB`

        Once uploaded, click on <sub><sub>![Root dir select example](../../../img/page-assets/user-guide/server-creation/minecraft/zip-import-root-dir-select.png)</sub></sub>

        ![root dir select example](../../../img/page-assets/user-guide/server-creation/minecraft/rood-dir-select-example.png)

        You will be directed to a selection screen. Here, you will select the directory you want as your root directory for your server. The root directory in this archive is the one base directory where all the server files are stored so I’ll just keep “Files” checked off.

!!! warning "Linux Users - Import Permissions"
    Due to Linux permissions you will not be able to import a server unless it has the file permissions mask/file ownership set correctly, otherwise Crafty essentially can't see it!

    Be sure the server directory your importing matches the following criteria:<br>
    - 1.  Owned by the '**crafty**' user -- `sudo chown crafty:crafty /path/to/your/server -R`
    <br>
    - 2.  Has a permissions mask of **2775** -- `sudo chmod 2775 /path/to/your/server -R`

    If you are importing from a directory such as `/home/bob/downloads`, please be very careful to not recursively change the permissions/ownership of your entire home folder, as you can imagine, **that would be very not fun...**
!!! warning "Docker/UnRAID Users - Import Location/Permissions"
    The only directory you can import servers from is the `/import` mount.<br>
    Drop a zip or directory in that mount point and restart the container to repair  permissions or if you'd rather not restart the container just ensure the files in import have the ownership (`<your local user>:root`)<br> Please refer to the [mount reference](../../getting-started/installation/docker.md#mount-locations) for further information.

    We'd recommend using the zip upload functionality of the panel 😎
## Starting a Server
You can start a server from the terminal by clicking the '**Start**' button or from the dashboard by clicking the :material-play: button.

If this is your first time starting the server you will be met with the dialogue, asking you to confirm you have read and agreed with [Minecraft's EULA](https://www.minecraft.net/en-us/eula){:target="_blank"}.
<center>
![Minecraft EULA Prompt](../../../img/page-assets/user-guide/server-creation/minecraft/minecraft-eula-prompt.png)
<br><small>If you press "**yes**", we will agree to the EULA and start the server for you.</small>
</center>

<center>
![Connectivity reminder prompt](../../../img/page-assets/user-guide/server-creation/minecraft/server-connectivity-reminder.png)
<br><small>Crafty will give a quick reminder to make sure your ports are forwarded if you want to access it remotely.</small>
</center>

-----

<center>
![Dashboard example with server running](../../../img/page-assets/user-guide/server-creation/minecraft/dashboard-server-running.png)
<br>**At this point, we have a running server! 🎉**
</center>
