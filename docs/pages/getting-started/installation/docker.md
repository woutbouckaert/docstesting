# Docker Installation Guide :simple-docker:

!!! warning "Docker on Windows - Docker Desktop / WSL"
    **BE ADVISED!** We do not support Docker for Windows! There is currently a known issue with volume permissions that will permanently destroy your Minecraft world if running on **Docker under WSL/WSL2, Windows 11 / DOCKER DESKTOP!**

    On **Stop** or **Restart** of the MC Server, there is a 90% chance the World's Chunks will be shredded irreparably!

    Please only run Docker on Linux based systems, If you are using Windows we have a portable installs, more information can be found here: [Windows Installation](windows.md)

With `Crafty 4` we have focused on building our DevOps Principles, implementing build automation, and securing our containers, with the hopes of making our Container user's lives abit easier.

**Two big changes introduced with Crafty 4:**

-   We now provide pre-built images for you.
-   Containers now run as non-root, using practices used by OpenShift &
    Kubernetes (root group perms).

This article will go slightly more in depth on how you can set up **Crafty** on Docker!<br>
(Remember you can find a quick start guide on the repositories README.md)

## Prerequisites

Docker is required. See the [official installation documentation](https://docs.docker.com/get-docker/){:target="_blank"}

## Prepare the bind mounts

Before we start getting into the meat things an important thing to know and understand is how we utilize volumes for persistent storage, if you are moving from a different install method to docker please customize you're paths accordingly!

### File permissions

To achieve host file acces in a *non-root* user environment we employ the *root group* file permissions method instead of requiring users to set a *UID* & *GID*.

To make the end users life easier in terms of volume and permissions we actively set permissions via a **Wrapper** __when the container starts for the first time__ (if no/empty config folder is present.)

This means any mounted volume in the container location `/crafty` will have
    - A `special bit` applied
    - `group perms` set to `root`

!!! info "Triggering automated mount permission repair"
    If you require the permissions to be set again, put files in the `/import` mount, then restart the container. This will trigger the wrapper to fix permissions over all bind mounts on container start, until `/import` is empty.

    {>>If your container is taking longer than normal to start make sure you haven't left files in the import directory.<<}

!!! danger
    **DO NOT** bind `'/'` (root) on your host system, as this will have grave consequence! If you don't feel comfortable with what you're doing stop and use the defaults below!

### Mount locations
When referring to file system locations in a containered Crafty instance, don't use host pathing as that wont exist in the container, please refer to the corresponding container location below:

| Local location   | Container location | Usage                                                            |
| :--------------- | :----------------- | :--------------------------------------------------------------- |
| ./docker/backups | /crafty/backups    | Storage for Server backups                                       |
| ./docker/logs    | /crafty/logs       | Storage for Crafty logs                                          |
| ./docker/servers | /crafty/servers    | Storage for Imported/Built Servers                               |
| ./docker/config  | /crafty/app/config | Storage for Crafty's config and database files                   |
| ./docker/import  | /crafty/import     | Where you will drop server zips/directories for crafty to import |

*RE: Imports --- We understand this is not ideal for everyone, you can upload smaller zips through the Web GUI*

## Installation

The Crafty Docker images can be deployed in multiple ways:

##### Using Docker CLI
!!! example "Docker CLI"
    === "Gitlab Registry"
        ``` shell
        docker run \
          --name crafty_container \
          --detach \
          --restart always \
          -p 8000:8000 \
          -p 8443:8443 \
          -p 8123:8123 \
          -p 19132:19132/udp \
          -p 25500-25600:25500-25600 \
          -e TZ=Etc/UTC \
          -v "/$(pwd)/docker/backups:/crafty/backups" \
          -v "/$(pwd)/docker/logs:/crafty/logs" \
          -v "/$(pwd)/docker/servers:/crafty/servers" \
          -v "/$(pwd)/docker/config:/crafty/app/config" \
          -v "/$(pwd)/docker/import:/crafty/import" \
          registry.gitlab.com/crafty-controller/crafty-4:latest
        ```

    === "Docker Hub Registry"
        ``` shell
        docker run \
          --name crafty_container \
          --detach \
          --restart always \
          -p 8000:8000 \
          -p 8443:8443 \
          -p 8123:8123 \
          -p 19132:19132/udp \
          -p 25500-25600:25500-25600 \
          -e TZ=Etc/UTC \
          -v "/$(pwd)/docker/backups:/crafty/backups" \
          -v "/$(pwd)/docker/logs:/crafty/logs" \
          -v "/$(pwd)/docker/servers:/crafty/servers" \
          -v "/$(pwd)/docker/config:/crafty/app/config" \
          -v "/$(pwd)/docker/import:/crafty/import" \
          arcadiatechnology/crafty-4:latest
        ```

    === "Local Image Build"
        If you'd rather not use the prebuilt image and assuming you have
        the repository cloned, in the root directory where the `Dockerfile` is
        located run use the following:
        ``` shell
        docker build . -t crafty-4

        docker run \
          --name crafty_container \
          --detach \
          --restart always \
          -p 8000:8000 \
          -p 8443:8443 \
          -p 8123:8123 \
          -p 19132:19132/udp \
          -p 25500-25600:25500-25600 \
          -e TZ=Etc/UTC \
          -v "/$(pwd)/docker/backups:/crafty/backups" \
          -v "/$(pwd)/docker/logs:/crafty/logs" \
          -v "/$(pwd)/docker/servers:/crafty/servers" \
          -v "/$(pwd)/docker/config:/crafty/app/config" \
          -v "/$(pwd)/docker/import:/crafty/import" \
          crafty-4
        ```

        A fresh build will take several minutes depending on your system, but will be rapid thereafter.

    -----
    The provided examples will pull/build the image, start Crafty in a container and publish ports needed for the Panel. '100 x Ports for Minecraft Servers, udp port for bedrock, and 8123 for dynmap(optional)'.

    All Crafty data will be stored in the `present working directory(pwd)` where you ran the `docker run` command The container will automatically restart after a system reboot.

##### Using Docker Compose
!!! example "Docker Compose"
    With [Docker Compose](https://docs.docker.com/compose/install/){:target="_blank"} you can easily deploy and update your containerized crafty instance!

    === "Pre-built image `docker-compose.yml`"

        ``` yaml linenums="1"
        version: '3'

        services:
          crafty:
            container_name: crafty_container
            image: registry.gitlab.com/crafty-controller/crafty-4:latest
            restart: always
            environment:
                - TZ=Etc/UTC
            ports:
                - "8000:8000" # HTTP
                - "8443:8443" # HTTPS
                - "8123:8123" # DYNMAP
                - "19132:19132/udp" # BEDROCK
                - "25500-25600:25500-25600" # MC SERV PORT RANGE
            volumes:
                - ./docker/backups:/crafty/backups
                - ./docker/logs:/crafty/logs
                - ./docker/servers:/crafty/servers
                - ./docker/config:/crafty/app/config
                - ./docker/import:/crafty/import
        ```
        ``` shell
        docker-compose up -d && docker-compose logs -f
        ```
    === "Local built image`docker-compose.yml`"
        If you are building from **docker-compose** you can find the compose file in [./docker/docker-compose.yml](https://gitlab.com/crafty-controller/crafty-4/-/blob/dev/docker/docker-compose.yml){:target="_blank"}

        Providing you have the repository cloned, just `cd` to the `/docker` directory where the build yml is included
        ``` shell
        docker-compose up -d --build && docker-compose logs -f
        ```

## Configuration

### Use tagged versions of Crafty
Arcadia produced images support both `arm64` and `amd64` out the box, if you have issues though you can build our image yourself with the local build examples above. Bare in mind some packages may require tweaking as a few are sometimes missing for certain architectures.
!!! bug "Support for third-party container images"
    As with any software, image variants built by third-parties come in many shapes and sizes.

    Please be advised if you are running a container that does not use our __**official image**__ we will not be able to provide you support. We can only support Arcadia images where we are familiar with the environment.

The image is located at:<br>
`registry.gitlab.com/crafty-controller/crafty-4:latest`<br>
`arcadiatechnology/crafty-4:latest`

If your require a specific version replace `latest` with the version you require, for a list of available tags: [Docker-Hub Registry](https://hub.docker.com/r/arcadiatechnology/crafty-4/tags){:target="_blank"} | [Gitlab Registry](https://gitlab.com/crafty-controller/crafty-4/container_registry/2952493){:target="_blank"}

### Expose Crafty on different ports

Crafty will require various ports to be exposed to be able to be accessible.<br>
The following is a list of the ports that are required:

  | Port          | Requirement                                 |
  | ------------- | ------------------------------------------- |
  | 8000          | Required for http > https redirects         |
  | 8443          | Required for Panel Access                   |
  | 8123          | Required for Dynmap(Optional)               |
  | 19132/udp     | Required for Bedrock Minecraft Server       |
  | 25500-25600   | Required Port range for Minecraft servers   |

*The port range for Minecraft servers has been reduced to 100 ports. This should be more than enough, you can of course increase this if required but remember higher ranges could cause the docker daemon's networking to stall.*
!!! info "Docker Port Syntax"
    If you are looking to re-map these ports it's as simple as changing the
    left most option(external) in the previous examples.

    ``` yaml
    ---Engine ---
    -p external:internal

    ---Compose---
    services:
    crafty:
        ports:
        - "external:internal";
    ```

    You can as well bind host networking as another option, This is pretty overkill and This will discard any published ports, It is useful to optimize performance in situations where a large range of ports is required...<br>
    Example: [Engine](https://docs.docker.com/network/host/){:target="_blank"} & Compose: `network_mode: host`<br>
    {>>You must keep in mind in the security implications of this as binding to the host does give dbus access!<<}

## Update Crafty Container

Updating to the newest version of crafty or switching should be easy as pi, in most instances, migrations of the database are handled internally by crafty.<br>

⚠️ *Always refer to update recommendations given by crafty staff when upgrading between major versions that may include breaking changes.* ⚠️

=== "Docker CLI"

    To update crafty that was installed using Docker Engine:

    1.  Take a backup of your [volume mounts](#mount-locations).
    2.  Stop the running container:
        ``` shell
        docker stop crafty_container
        ```
    3.  Remove the existing container:
        ``` shell
        docker rm crafty_container
        ```
    4.  Pull the new image or build from repo. For example, the latest
        Crafty image:

        -   Pre-built
            ``` yaml
            docker pull registry.gitlab.com/crafty-controller/crafty-4:latest
            ```
        -   Local-build
            ``` shell
            git fetch && git pull && docker build . -t crafty
            ```

    5.  Create the container once again with the [previously specified
        options](#using-docker-cli)

=== "Docker Compose"

    To update Crafty that was [installed using Docker
    Compose](#using-docker-compose):

    1.  Take a backup of your [volume mounts](#mount-locations).
    2.  Download the newest/build release and update your Crafty instance:

    -   Pre-built
        ``` shell
        docker-compose pull && docker-compose up -d
        ```
    -   Local-build
        ``` shell
        git fetch
        git pull
        docker-compose build
        docker-compose up -d && docker-compose logs -f
        ```

=== "Watchtower"
    Docs coming soon! <!-- -TODO Write watchtower docs -->

## Back up Crafty

Crafty does not have any built in backup/restore methods currently, This is something that you will need to do manually, or using automation of your choice! It should be as simple as backing up your [volume mounts](#mount-locations)!

-----

## Post-Install
<center>
Excellent, by this point you should have successfully stood up your crafty instance!<br>
Great work, You're ready to proceed to the next step 🎉<br>
[How to access the dashboard :material-application-import:](../access.md){ .md-button }
</center>

## Troubleshooting / FAQ

The following information may be of some use if you encounter problems using Crafty and Docker

### Java paths inside the container

As the container comes pre-built with most required versions of Java, you will need to select the correct one for the server version that you are using.

The following table will detail the Java versions and their locations:
=== "amd64"
    | MC Version             | Java Version   | Container location                          |
    | ---------------------- | -------------- | ------------------------------------------- |
    | Pre-1.16.5             | Java 8         | /usr/lib/jvm/java-8-openjdk-amd64/bin/java  |
    | 1.16.5 vanilla/Forge   | Java 11        | /usr/lib/jvm/java-11-openjdk-amd64/bin/java |
    | 1.17 +                 | Java 17        | /usr/lib/jvm/java-17-openjdk-amd64/bin/java |
=== "arm64"
    | MC Version             | Java Version   | Container location                          |
    | ---------------------- | -------------- | ------------------------------------------- |
    | Pre-1.16.5             | Java 8         | /usr/lib/jvm/java-8-openjdk-arm64/bin/java  |
    | 1.16.5 vanilla/Forge   | Java 11        | /usr/lib/jvm/java-11-openjdk-arm64/bin/java |
    | 1.17 +                 | Java 17        | /usr/lib/jvm/java-17-openjdk-arm64/bin/java |

### Diagnose potential problems

-   Read container logs:
    ``` shell
    docker logs crafty_container -f
    ```
-   Enter running container:
    ``` shell
    docker exec -it crafty_container bash
    ```

    From within the container you can administer the Crafty container as you would normally administer on a bare metal instance, bare in mind though crafty runs in `daemon mode` in the container and will not accept commands.

### Permission problems

When updating from legacy Crafty Docker images, or transferring files in and out of the volume mounts, you might counter permission problems.

If this happens just restart the container and the wrapper should correct the permissions on the volume mounts. If the wrapper is unable to resolve this, ensure the file permissions on your host are `<your local user>:root` for the [bind mounts](#mount-locations).

### Minecraft World Chunk Corruption

On restarting a Minecraft server inside of crafty if you are receiving a whole bunch of chunk corruptions/truncation, You are likely running on `Docker for Windows` This is not supported by our image and other Minecraft images due to volume mount issues with `Docker for Windows`.

We mentioned this at the start of this documentation and on the quick start README.md, And don't have much information on how you can recover worlds affected by this. If you are using `Docker for Windows` it is unsupported, ⚠️Proceed at your own risk⚠️

### Error during connection

The following error is usually indicated when there is an issue with the docker daemon or it is not running, Please ensure your docker daemon is running and restart if required.

``` shell
error during connect: This error may indicate that the docker daemon is not running.: Get &quot;http://%2F%2F.%2Fpipe%2Fdocker_engine/v1.24/containers/json&quot;: open //./pipe/docker_engine: The system cannot find the file specified.
```

### How to Setup Forge
!!! success "Note"
    We recommend using 'Crafty's Server Builder Tool' to create a Forge server
!!! info "Select the correrct Java version for Forge"
    Java is packaged with our image, you will need to ensure you select the correct version to run Forge 1.18 or 1.19
    Please refer to the Version table

Here are the steps to get Forge server running:

-   **STEP 1** Run Forge Installer, select directory like `/forge` to extract to. After doing so zip the directory `/forge` to a archive called `forge.zip` for example.

-   **STEP 2** Copy this zip file to  `<docker dir>/docker/import` and return to the 'Crafty Dashboard'.

-   **STEP 3** Click Create a server, and follow the instructions in zip
    import. (The Import should be pretty quick.)

    | Import field             | Example                                                                            |
    | ------------------------ | ---------------------------------------------------------------------------------- |
    | Server Name              | `Forge Test`                                                                       |
    | Server path              | `/crafty/import/forge.zip`                                                         |
    | Select root dir          | `Select 'forge' or whatever the parent directory of /libraries is.`                |
    | Server Executable file   | `libraries/net/minecraftforge/forge/1.18.2-40.1.68/forge-1.18.2-40.1.68-server.jar`|
    | Min Memory               | `4GB (or whatever)`                                                                |
    | Max Memory               | `4GB (or whatever)`                                                                |
    | Server Port              | `<Your choice, I left as default 25565>`                                           |

-   **STEP 4** When the forge server has been imported we are going to
    need to do a little bit of deconstruction of the **.sh file**
    created by the Forge installer.

**Your Server Execution Command will look something like this:**<br>
    ```
    java -Xms4000M -Xmx4000M @libraries/net/minecraftforge/forge/1.18.2-40.1.68/unix_args.txt nogui
    ```
    ***DO NOT just copy the above, your forge server will be different. LOOK at your .bat/.sh provided by the installer and extrapolate the information to resemble the above. (No need to include %* or $@)***

-   **STEP 5** You should now be good to start the server, it will take
    a moment for information to display on the **Terminal** because
    Forge servers are chonkers.

## Help and Feedback
!!! question "Help and Feedback"
    Hi there *Zedifus* here! I've tried to be as comprehensive as possible writing this documentation.<br> If you require any further help, please reach out to us on [Discord](https://discord.gg/9VJPhCE){:target="_blank"} for non-commercial, and <support@business.arcadiatech.org> for commercial support.

    If you have any feedback related to this, or have any changes you would like to see please do let us know!
