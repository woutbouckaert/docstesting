!!! info "Please Note"
    Developer Guide docs are largely incomplete.<br><br>
    **Check Back Later!**

    In the meantime please use the old docs: [API v2 Documentation Reference](https://wiki.craftycontrol.com/en/4/docs/API%20V2){:target="_blank"}
