# Uninstall Guide :material-harddisk-remove:

We're sorry to see you go! 😭

If you've decided to uninstall Crafty, we'll guide you through the process.

Follow the steps below to remove Crafty from your system:

=== "Linux :simple-linux:"
    !!! example "Crafty Service File"
        If you chose to add a service file during the installation, run the following commands to remove the Crafty service:
        ```
        sudo systemctl stop crafty
        sudo systemctl disable crafty
        sudo rm /etc/systemd/system/crafty.service
        sudo systemctl daemon-reload
        sudo systemctl reset-failed
        ```
    **Uninstallation Steps**

    1. **Ensure Crafty is not running**: Before uninstalling, make sure that Crafty is not running. Stop any running instances or services related to Crafty.

    2. **Backup all servers**: It's recommended to create backups of all your Crafty servers before proceeding with the uninstallation. This will ensure that you have a copy of your server data in case you need it in the future.

    3. `sudo rm -rf /var/opt/minecraft`: Remove the Crafty server data directory located at `/var/opt/minecraft`. This will delete all server files and configurations associated with Crafty.

    4. `sudo deluser crafty`: Delete the Crafty user account. This will remove the user created for Crafty during installation.

    By following these steps, you have successfully uninstall Crafty from your system.

=== "Docker :simple-docker:"

    **Uninstallation Steps**

    1. **Stop and remove the containers**: First, stop and remove the Crafty container running on your Docker host. Open a terminal or command prompt,

        === "Docker CLI"
            ```shell title="Run the following commands to stop and remove the Crafty container:"
            docker stop crafty_container
            docker rm crafty_container
            ```
        === "Docker Compose"
            Navigate to the directory where your Docker Compose file is located.
            ```shell title="Run the following command to stop and remove the Crafty container:"
            docker compose down
            ```
    2. **Remove the Crafty container volume**: Crafty will have created a Docker volume to store persistent system data.

        ```shell title="To remove this volume, run the following command:"
        docker volume rm crafty_container
        ```

    3. **Remove Crafty files**: Delete the Crafty application files from your system.

        **⚠️ Don't do this if you wish to retain your server files ⚠️**<br>
        The path will vairy depending on where you ran `docker run` or where the `docker-compose.yml` file is.

        ```shell title="You can simply delete the Crafty data directory by running:"
        rm -rf /path/to/crafty
        ```

    4. **Remove Docker images**: If you no longer need the Crafty Docker image, you can remove it by running the following command:

        ```shell title="This command removes the Crafty Docker image from your system."
        docker rmi crafty-4
        ```

    By following these steps, you can successfully remove the Crafty application and associated Docker resources from your system.

=== "Windows :simple-windows:"
    As the installation on Windows systems is portable there is not much to it:

    **Uninstallation Steps**

    - Ensure crafty is not running,
    - Backup your server files,
    - Remove the directory that contains `crafty.exe`

    By following these steps, you have successfully uninstall Crafty from your system.

=== "MacOS :simple-apple:"
    As there is not much to the installation on MacOS systems, there is not much to it:

    **Uninstallation Steps**

    - Ensure crafty is not running,
    - Backup your server files,
    - `rm -rf /var/opt/minecraft`

    This will remove all crafty related files.
    !!! example ""
        If you wish to remove the install dependencies like [Brew](macos.md#brewsh), it is very similar to the way it is installed.

        ``` bash title="Open up your terminal and paste the following command:"
        /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/uninstall.sh)"
        ```
    By following these steps, you have successfully uninstall Crafty from your system.

-----

If you have any questions or encounter any issues, please refer to this documentation or [reach out to our support team](../../developer-guide/contributing/ask-a-question.md) for further assistance.

Thank you for using Crafty, and we appreciate your time with our platform! 💖
