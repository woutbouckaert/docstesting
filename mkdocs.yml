# Page tree
nav:
  - Home: index.md
  - Getting started:
      - Crafty Compatibility: pages/getting-started/compatibility.md
      - Installation:
          - Docker: pages/getting-started/installation/docker.md
          - Linux: pages/getting-started/installation/linux.md
          - Windows: pages/getting-started/installation/windows.md
          - MacOS: pages/getting-started/installation/macos.md
          - UnRAID: pages/getting-started/installation/unraid.md
          - Uninstalling Crafty: pages/getting-started/installation/uninstalling.md
      - First Steps:
          - Access the Dashboard: pages/getting-started/access.md
          - Configure Crafty: pages/getting-started/config.md
          - Reverse Proxies: pages/getting-started/proxies.md
          - Public Status Page: pages/getting-started/public-status-page.md
          - Progressive Web App: pages/getting-started/pwa.md
  - User Guide:
      - User/Role Configuration: pages/user-guide/user-role-config.md
      - Server Creation:
          - Minecraft Server: pages/user-guide/server-creation/minecraft.md
          #          - SteamCMD Server(Beta): pages/user-guide/server-creation/steamcmd.md
      - Server Configuration:
          - Minecraft Server: pages/user-guide/server-config/minecraft.md
          #          - SteamCMD Server(Beta): pages/user-guide/server-config/steamcmd.md
      - Server Management:
          - Server File Manager: pages/user-guide/file-manager.md
          - Server Backup Manager: pages/user-guide/backup-manager.md
          - Server Metrics: pages/user-guide/metrics.md
          - Server Task Scheduler: pages/user-guide/task-scheduler.md
          - Server Re-ordering: pages/user-guide/reorder-servers.md
          - Server Webhooks: pages/user-guide/webhooks.md
      - Troubleshooting (FAQ): pages/user-guide/faq.md
  - Developer Guide:
      - API Reference (v2): pages/developer-guide/api-reference/v2.md
      - API Reference (v1): pages/developer-guide/api-reference/v1.md
      - Contributing:
          - pages/developer-guide/contributing/index.md
          - Making repository changes: https://gitlab.com/crafty-controller/crafty-4/-/blob/master/CONTRIBUTING.md
          - Reporting a bug: pages/developer-guide/contributing/bug-reporting.md
          - Reporting a docs issue: pages/developer-guide/contributing/doc-issue.md
          - Requesting a change: pages/developer-guide/contributing/change-requests.md
          - Asking a question: pages/developer-guide/contributing/ask-a-question.md
  - About:
      - Credits: pages/about/credits.md
      - Licence: pages/about/licence.md

# Project paths.
docs_dir: docs
site_dir: site

# Project information.
site_name: Crafty Documentation
#site_url: https://docs.craftycontrol.com/
site_description: >-
  Crafty 4's Technical Documentation & API Reference
site_author: Arcadia Technology
#use_directory_urls: true

# Repository information.
repo_name: crafty-controller/crafty-4
repo_url: https://gitlab.com/crafty-controller/crafty-4
edit_uri: https://gitlab.com/crafty-controller/crafty-documentation/-/tree/main/docs
copyright: © 2023 Arcadia Technology. All Rights Reserved.

# Theme configuration.
theme:
  name: "material"
  logo: img/favicon.ico
  palette:
    # Palette toggle for light mode
    - media: "(prefers-color-scheme: light)"
      scheme: default
      toggle:
        icon: material/brightness-7
        name: Switch to dark mode

    # Palette toggle for dark mode
    - media: "(prefers-color-scheme: dark)"
      scheme: slate
      toggle:
        icon: material/brightness-4
        name: Switch to light mode

  favicon: img/favicon.ico
  icon:
    logo: img/favicon.ico

  # Default values, taken from mkdocs_theme.yml
  language: en
  features:
    - content.action.view
    - content.code.annotate
    - content.code.copy
    - content.tabs.link
    - content.tooltips
    - navigation.footer
    - navigation.indexes
    - navigation.sections
    - navigation.tabs
    - navigation.top
    - navigation.tracking
    - search.highlight
    - search.share
    - search.suggest
    - toc.follow

  font:
    text: Roboto
    code: Roboto Mono

# External links.
extra:
  generator: false
  homepage: https://craftycontrol.com/
  social:
    - icon: fontawesome/brands/gitlab
      link: https://gitlab.com/crafty-controller/crafty-4
    - icon: fontawesome/brands/docker
      link: https://hub.docker.com/r/arcadiatechnology/crafty-4
    - icon: fontawesome/brands/patreon
      link: https://www.patreon.com/craftycontroller
    - icon: material/coffee
      link: https://ko-fi.com/arcadiatech
    - icon: fontawesome/brands/discord
      link: https://discord.gg/9VJPhCE
    - icon: material/home
      link: https://craftycontrol.com/

plugins:
  - search:
      separator: '[\s\-,:!=\[\]()"`/]+|\.(?!\d)|&[lg]t;|(?!\b)(?=[A-Z][a-z])'
  - render_swagger
  #- external-markdown

markdown_extensions:
  - admonition
  - attr_list
  - def_list
  - md_in_html
  - pymdownx.critic
  - pymdownx.details
  - pymdownx.emoji:
      emoji_index: !!python/name:material.extensions.emoji.twemoji
      emoji_generator: !!python/name:material.extensions.emoji.to_svg
  - pymdownx.highlight:
      anchor_linenums: true
      line_spans: __span
      pygments_lang_class: true
  - pymdownx.inlinehilite
  - pymdownx.keys
  - pymdownx.snippets
  - pymdownx.superfences
  - pymdownx.tabbed:
      alternate_style: true
  - pymdownx.tasklist:
      custom_checkbox: true
  - tables
