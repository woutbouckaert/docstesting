---
hide:
  - navigation
  - toc
---

<p style="text-align: center; margin-left: 1.6rem;">
  <img alt="Crafty Controller logo" src="./img/logo_long.svg" width="800" />
</p>
#
<p align="center">
  A Web based GUI for Minecraft Server administration.<br> <sub>Boasting a clean new look, rebuilt from the ground up. Crafty 4 brings a whole host of new features such as Bedrock support. With SteamCMD support on the way!</sub>
  <br/><br/>
  <a href="https://github.com/psf/black" target="_blank">
    <img alt="Code style: black" src="https://img.shields.io/badge/code%20style-black-000000.svg" />
  </a>
  <a href="https://www.python.org" target="_blank">
    <img alt="Supported Python Versions" src="https://shields.io/badge/python-3.9%20%7C%203.10%20%7C%203.11%20-blue">
  </a>
  <a href="https://gitlab.com/crafty-controller/crafty-4/-/releases" target="_blank">
    <img alt="Crafty Version" src="https://img.shields.io/gitlab/v/tag/20430749?label=release" />
  </a>
  <a href="https://gitlab.com/crafty-controller/crafty-commander/-/commits/master" target="_blank">
    <img alt="main Build Status" src="https://gitlab.com/crafty-controller/crafty-commander/badges/master/pipeline.svg" />
  </a>
  <a href="https://gitlab.com/crafty-controller/crafty-4/-/blob/master/LICENSE" target="_blank">
    <img alt="GNU General Public License v3.0 or later" src="https://img.shields.io/gitlab/license/20430749" />
  </a>
</p>
