# Linux Installation Guide :simple-linux:

## Automated Install Script (Quick)

- Please ensure your distro's packages are up-to-date and Git is present before installing Crafty:

    ``` shell
    sudo apt update && sudo apt upgrade && sudo apt install git
    ```

- One line installer

    ``` shell
    git clone https://gitlab.com/crafty-controller/crafty-installer-4.0.git && \
     cd crafty-installer-4.0 && \
     sudo ./install_crafty.sh
    ```

- Once installation is complete to run Crafty manually run the following. Note that if you used a directory other then the default to install Crafty you will need to `cd` into that directory.

    ``` shell
    sudo su crafty
    ```
    ``` shell
    cd /var/opt/minecraft/crafty
    ```
    ``` shell
    ./run_crafty.sh
    ```
??? example "Crafty Service File"
    If you said yes to adding a service file you may run the following to
    start Crafty

    ``` shell
    sudo systemctl start crafty
    ```

    If you added a service file and want to have Crafty start automatically
    on system boot please run the following:

    ``` shell
    sudo systemctl enable crafty
    ```

## Manual installation steps (for Ubuntu/Debian, CentOS)
!!! abstract "Crafty Requirements"
    -   You have sudoer user that can install software.
    -   This guide assumes you have a decent understanding of a Linux environment and are comfortable with the command line.
    -   This guide also assumes you will be installing Crafty into `/var/opt/minecraft/crafty`.
    -   We also assume your server will be at `/var/opt/minecraft/server`.
    -   We also assume the user account running crafty will have full read/write/execute permissions on these folders.
    -   This guide will instruct you how to setup a service account for Crafty as well.
    -   Finally this guide assumes you have the following software installed and up to date: **Git, Python 3.9+, Python 3.9-dev+, python3-pip**.

    NOTE: You will need to install **Java** before starting any **Minecraft** servers.


### Installation

#### Install host dependancies

=== "Ubuntu/Debian"
    ``` shell
    sudo apt install git python3 python3-dev python3-pip software-properties-common openjdk-8-jdk openjdk-8-jre
    ```

=== "CentOS"
    ``` shell
    sudo dnf groupinstall "Development tools" && \
    sudo dnf install python3 python3-devel java-1.8.0-openjdk java-1.8.0-openjdk-devel
    ```

#### Create a Crafty User Account

We'll want to create a service user for crafty.
    ``` shell
    sudo useradd crafty -s /bin/bash
    ```

#### Create a directory for Crafty

``` shell title="Let's make crafty a place to live on your server."
sudo mkdir -p /var/opt/minecraft/crafty /var/opt/minecraft/server
```
``` shell title="Make the directories owned by the 'crafty' service user"
sudo chown -R crafty:crafty /var/opt/minecraft
```
``` shell title="Once created lets change our working directory to our new installation dir"
cd /var/opt/minecraft/crafty
```

#### Clone the Crafty Repository

*Please be sure to be in `/var/opt/minecraft/crafty` folder before cloning the repo.*<br>
{>>To check type 'pwd' and make sure it says '/var/opt/minecraft/crafty' before you continue.<<}

``` shell title="Clone the Repo"
git clone https://gitlab.com/crafty-controller/crafty-4.git
```

#### Create a Virtual Environment
``` shell title="Make venv called '.venv'"
python3 -m venv .venv # (1)!
```


1.  We prefix a `.` to the name of the `virtual environment` because in a Linux/Unix context it makes that the file or directory hidden, you cannot see it in the file explorer (`ls`) unless the `-a` flag is provided.

#### Activate the Virtual Environment
``` shell title="[Sudo User] Switch to the 'crafty' Service User"
sudo su crafty -
```
``` shell title="[Crafty User] Activate the venv"
source .venv/bin/activate # (1)!
```

1.  You know when you're in your virtual environment because your prompt will change to include `(venv)`

#### Install Crafty's package dependancies
``` shell title="[Crafty User](venv) Change to the crafty-4 dir that was cloned down"
cd /var/opt/minecraft/crafty/crafty-4
```
``` shell title="[Crafty User](venv) Install Crafty's required packages"
pip3 install --no-cache-dir -r requirements.txt
```

### Run Crafty

Make sure you're still in your `venv` and in the `/var/opt/minecraft/crafty/crafty-4` directory.<br>
{>>To check type 'pwd' and make sure it says '/var/opt/minecraft/crafty/crafty-4' before you continue.<<}
``` shell title="[Crafty User](venv) Run Crafty inside the virtal environment"
python3 main.py
```

**That's it! Crafty should now be running and asking some install questions. 🎉**<br>
If you get stuck or [Need help? Click here!](../../developer-guide/contributing/ask-a-question.md)

### Updating Crafty

!!! warning "Make sure to stop Crafty before updating"

    === "systemd"
        ``` shell title="If you are on linux running as a systemd service, type:"
        sudo systemctl stop crafty
        ```

    === "Terminal / CLI"
        If you are running Crafty in a terminal or CMD window, use:

        ++control+c++
!!! info "If you experience git telling you to commit or stash local changes and you have not made any changes"
     In the `/var/opt/minecraft/crafty/crafty-4` directory, run the following:
     ``` shell
     git reset --hard origin/master
     ```
     If you have made changes you might want to stash them, but popping them could cause adverse effects.<br> {>>Make sure to run the the following update commands after running this.<<}


=== "Update After Automated Install Script"
    ``` shell
    cd /var/opt/minecraft/crafty
    ```
    ``` shell
    sudo su crafty
    ```
    ``` shell
    ./update_crafty.sh
    ```

=== "Update After Manual Install"

    ``` shell
    cd /var/opt/minecraft/crafty/crafty-4
    ```
    ``` shell
    sudo su crafty
    ```
    ``` shell
    git pull
    ```
    ``` shell
    cd ..
    ```
    ``` shell
    source .venv/bin/activate
    ```
    ``` shell
    cd crafty-4
    ```
    ``` shell
    pip3 install -r requirements.txt --no-cache-dir
    ```

-----

## Post-Install
<center>
Excellent, by this point you should have successfully stood up your crafty instance!<br>
Great work, You're ready to proceed to the next step 🎉<br>
[How to access the dashboard :material-application-import:](../access.md){ .md-button }
</center>
