# Using Server Webhooks :octicons-webhook-16:

This document provides a detailed guide on using Crafty's server webhooks to dispatch notifications. Webhooks are user-defined HTTP callbacks that you can use to notify you when certain server events happen, making them crucial for real-time updates.

With Crafty, you can set up webhooks to send notifications to various platforms like Slack, Mattermost, Discord, and Teams, with more integrations planned for the future.

This document will walk you through the basics of configuring webhooks, setting up notification providers, and customizing messages for different services.

## Setting Up Server Webhooks <sub><small>[:octicons-tag-24: 4.2.0][Webhooks Release]{:target="_blank"}</small></sub>
![Server Webhook Overview](../../img/page-assets/user-guide/server-webhooks/webhook-overview.png)
<small>
#### Notification Provider
Choose the provider you want to integrate with from the available list. Expect more of these to be added in the future!
!!! note "API Limitations"
    Be mindful of the rate limits imposed by the services you are integrating with. Exceeding these limits could result in delayed or lost notifications.

#### Webhook Name
This will be used in the dispatched webhook as the title of the notification

#### Webhook URL
The call-back URL of the provider you have selected, make sure to match this up with the correct provider, or you may recieve some un expected error messages.

#### Bot Name
The user name you'd like the message to appear from in your provider, this only works on certain providers and may need manually set in your providers webhook settings.

#### Event Triggers
Select the events that will trigger notifications. Whether it's server start-ups, shut-downs, or user-defined events, you can control what gets announced and when.

#### Customise Messages
Tailor the notification messages to suit your audience. For most providers you can use `markdown` formatting and soon you'll be able to include dynamic variables from Crafty's server environment.

#### Colour Accent
Some providers support a colour accent, such as a strip of colour on the notification message, customise this with the provied colour picker, or leave as pretty blue 😊

#### Test Notification
Be sure to send a test notification to verify that your webhook is configured correctly and the message appears as intended on your chosen provider.
</small>

-----

<center>

### Webhook Actions

![Webhook Actions](../../img/page-assets/user-guide/server-webhooks/webhook-actions.png)

##### :material-plus-circle: New Webhook
<small>Select and configure a new service to integrate with your server.</small>

##### Test Webhook 🧪
<small>Click this to send a test notification with your current configuration.</small>

##### Edit Message Template ✏️
<small>Customise the notification message for each service.</small>

##### Delete Service 🗑️
<small>Remove an integration if it's no longer needed.</small>

</center>

[Webhooks Release]: https://gitlab.com/crafty-controller/crafty-4/-/releases/v4.2.0
