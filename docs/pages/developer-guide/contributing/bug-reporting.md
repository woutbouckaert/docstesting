# Reporting a Bug :material-shield-bug:

We strive to provide you with a smooth and bug-free experience while using Crafty. However, if you encounter any issues or bugs, we appreciate your help in reporting them. By doing so, you contribute to the ongoing improvement of our platform.

To report a bug, please visit our [Crafty GitLab repository](https://gitlab.com/crafty-controller/crafty-4/-/issues/new){:target="_blank"} and create a new issue. Be sure to select the "**Bug**" issue template to provide us with the necessary information.

The issue template will provide the following sections to structure your issue:
!!! example ""
    - **Quick Information:**
        - **Operating System:** Windows / Linux / MacOS / UnRAID
        - **Install Type:** Git Cloned (Manual) / Installer / WinPackage / Docker

    - **What Happened?**
        *A brief description of what happened when you tried to perform an action.*

    - **Expected Result**
        *What should have happened when you performed the actions.*

    - **Steps to Reproduce**
        *List the steps required to produce the error. These should be as few as possible.*

    - **Screenshots**
        *Any relevant screenshots which show the issue.*

    - **Priority/Severity**
        - [ ] High (anything that impacts the normal user flow or blocks app usage)
        - [ ] Medium (anything that negatively affects the user experience)
        - [ ] Low (anything else e.g., typos, missing icons/translations, layout/formatting issues, etc.)

Once you have filled in the necessary information, submit the issue, and our team will review it promptly.

Your active participation in reporting bugs helps us maintain a high-quality platform and ensures a smoother experience for all users.

Thank you for your support,<br>
The Crafty Team 💖
