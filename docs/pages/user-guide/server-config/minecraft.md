# Minecraft Server Configuration :material-minecraft: :material-wrench-cog:

This document provides detailed explanations of options used to configure various aspects of your server within Crafty.

It covers a wide range of options, including server name, working directory, log location, executable and execution command, stop command, auto start delay, executable update URL, server IP, server port, shutdown timeout, crash detection settings, log retention, auto start feature, public status page visibility, and more.

By following this document, you'll gain a comprehensive understanding of each configuration option and how it affects your server. Whether you're a Minecraft Java player or managing other game servers, this documentation will help you customize and optimize your server setup according to your requirements.

![Server config overview](../../../img/page-assets/user-guide/server-configuration/minecraft-config-overview.png)

| Option                         | Description                                                                                     |
|--------------------------------|-------------------------------------------------------------------------------------------------|
| Server Name                    | The display name for your server within Crafty (local to Crafty).                               |
| Server Working Directory       | The directory where your server is stored. This field is *view-only*, but the master server's storage location can be changed in the [panel settings document](../../getting-started/config.md#changing-the-server-storage-location). |
| Server Log Location            | The path to the `latest.log` file of your server. This path can be *relative* to the server's working directory. |
| Server Executable              | The name of the *executable file* that Crafty will launch when you press the start button.        |
| Server Execution Command       | The *command* used by Crafty to start your server.                                                |
| Server Stop Command            | The *command* used by Crafty to stop your server.                                                 |
| Server Auto Start Delay        | The *delay duration* in seconds before Crafty automatically starts your server after Crafty starts. |
| Server Executable Update URL   | The *direct download link* for updating the server executable file. Only applicable to Minecraft Java servers. |
| Server IP                      | The *IP address* that Crafty will use to poll your server's statistics.                            |
| Server Port                    | The *port number* that Crafty will use to poll your server's statistics.                           |
| Shutdown Timeout               | The *maximum time* in seconds that Crafty will wait for your server to shut down before declaring it as hung and terminating the process. |
| Ignored Crash Exit Codes       | The *exit codes* that Crafty's crash detection feature will ignore, treating them as "normal" exits. |
| Remove Old Logs After          | The *number of days* Crafty should keep server log files before automatically removing them.        |
| Server Auto Start              | Whether Crafty should *automatically start* your server when Crafty starts.                        |
| Server Crash Detection         | Whether Crafty should *attempt to restart* your server after a crash.                               |
| Show On Public Status Page     | Whether Crafty should *display this server* on the [public status page](../../getting-started/public-status-page.md) (accessible at `https://ip:8443/status`). |
| Buttons                        |                                                                                                 |
| - Delete                       | This button *deletes your server*. It prompts for confirmation and offers to delete associated files and backups. |
| - Update Executable            | This button *updates the server executable file*, including a backup. For Minecraft Java, a direct download link must be provided. See the Server Executable Update URL option. |
