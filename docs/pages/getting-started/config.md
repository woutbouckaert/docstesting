# Configure your Crafty Instance :material-wrench-cog:

This document provides instructions for configuring Crafty, before launching it or for existing installations.

Whether you're setting up Crafty for the first time or want to optimise your existing configuration, this document covers essential topics such as port configuration, admin user credentials, optimisation, and more.

By following these instructions, you can ensure that Crafty runs smoothly and securely.<br> Whether you're a developer, a system administrator, or a gamer just out to game with your mates, this document has something to offer to help you get the most out of the Crafty.

## Launch Options
| Flag | Long form  | Description                                                                                                        |
|------| ---------- |------------------------------------------------------------------------------------------------------------------  |
| -v   | --verbose  | Sets logging level to 'DEBUG'.<br>*(Only use when neccessary! i.e. seeking support, the logs can get massive!)*    |
| -d   | --daemon   | Runs Crafty in `daemon mode` (no prompt)                                                                           |
| -i   | --ignore   | Ignore session.lock file.<br><br>*Crafty protects itself from running multiple instances with a lock file, As multiple craftys running at the same time would break files and you would have a real bad time. Sometimes this can cause complications for example if you're running in an environment where crafty will not always shutdown gracefully (in a container). Be sure to only use this in an environment where you can be sure with utmost certainty only one crafty will be ran.* |

## Crafty's Config File (config.json)
This file is Crafty's system configuration file, it provides numerous settings which will help you configure and get the best performance out of Crafty.

!!! note ""
    You can find it in `app/config/config.json`, the `config/config.json` mount point in docker or in the **dashboard** as a 'superuser'.
![Crafty config file editor](../../img/page-assets/getting-started/config/crafty-4-config-json.png)
??? info "Crafty System Configurable Options w/ Descriptions"
    | Key                              | Type      | Default | Description                                                                          |
    | -------------------------------: | :-------: | :------ | :----------------------------------------------------------------------------------- |
    | "allow_nsfw_profile_pictures"    | bool      | false   | Whether or not to allow gravatar defined NSFW PFPs for your local users.             |
    | "cookie_expire"                  | int       | 30      | How long cookies are valid (days)                                                    |
    | "crafty_logs_delete_after_days"  | int       | 0       | How we should keep crafty logs for (days, 0 is never delete)                         |
    | "delete_default_json"            | bool      | false   | Whether to use [default.json](#defaultjson) when starting Crafty for the first time. |
    | "dir_size_poll_freq_minutes"     | int       | 5       | How often we should poll directories for their size (minutes)<br>(The more this happens the performance intensive Crafty will be) |
    | "disabled_language_files"        | list[str] | [ ]      | Which languages you would like disabled for users.                                   |
    | "enable_user_self_delete"        | bool      | false   | Can a user delete themselves.                                                        |
    | "history_max_age"                | int       | 7       | How long we should hold onto stats in the database for. (days)                       |
    | "http_port"                      | int       | 8000    | Port for our http redirect server                                                    |
    | "https_port"                     | int       | 8443    | Port for Crafty's main secure webserver.                                             |
    | "keywords"                       | list[str] | [ <br>"help",<br>"chunk"<br> ] | Keywords that will be highlighted in the terminal and in the logs page. |
    | "language"                       | str       | "en_EN" | Default Crafty instance language.<br>(This is for login page and any public pages with translations enabled) |
    | "max_audit_entries"              | int       | 300     | How many audit log entries we will keep in the DB.                                   |
    | "max_log_lines"                  | int       | 700     | How many lines of logs we will keep on the logs page.                                |
    | "monitored_mounts"               | str       | "C:\\"  | Mount points that will appear under 'storage monitoring' on the dashboard.           |
    | "reset_secrets_on_next_boot"     | bool      | false   | Should we reset the key secrets on the next boot?                                    |
    | "show_contribute_link"           | bool      | true    | Show the link to contributors on the navigation of Crafty.                           |
    | "show_errors"                    | bool      | true    | Should we show traceback errors on the page<br>(This helps us with debugging when you come in with issues. 👷‍♂️) |
    | "stats_update_frequency_seconds" | int       | 30      | How often we poll your server for stats. (seconds)                                   |
    | "stream_size_GB"                 | int       | 1       | Max upload size (in GB)                                                              |
    | "virtual_terminal_lines"         | int       | 70      | How many lines we should save in your server's terminal buffer.                      |

## Changing the server storage location
![Server location config option example](../../img/page-assets/getting-started/config/crafty-4-server-location-config.png)
When you create or import a server, Crafty automatically stores the server data in an internal managed directory.

If you prefer to customize the storage location, you can easily do so in the **'Panel Config'** tab.

Simply set the desired path where you want your Crafty servers to be located. It's important to ensure that Crafty has the necessary **read/write permissions** for this directory.

For detailed instructions on configuring the server storage location for Linux, please refer to [this document](./installation/linux.md#create-a-directory-for-crafty). It provides an example of how the directory should be created.

!!! info "If you are on Docker you won't be able to change this path, use the mount points instead. ([Mount locations](./installation/docker.md#mount-locations))"

## Configuring before you launch
If you need to change that port or would like to create the config file before Crafty starts you may do so by creating the [config.json detailed above](#craftys-config-file-configjson) before you launch crafty.

**Just be sure you get the json syntax correct formatting right or crafty won't be happy with you.**
!!! question ""
    If you do not need to change your server port before starting Crafty have no fear... Crafty will create this config file for you. Just start the web app up.
### Changing Crafty's port
Here you can find an example of how to update what ports crafty runs on prior to launch.

If you are on linux and creating this file for the first time **please, please, please**, make sure you are doing this as the Crafty user (`sudo su - crafty`).

``` json title="The new json file should be formatted as follows:"
{
  "http_port": 8000,
  "https_port": 8443
}
```

### What is 'default.json' 🤔
!!! note ""
    You can find it in `app/config/default.json.example`, the `config/default.json.example` mount point in docker.
This is a *special file* ✨ that is injested and deleted when crafty launches for the first time, you can use it to define the admin user credentials. Useful for when you want to do unattended deployments of Crafty, Like with Ansible! <sub>*#andible-roles-may-or-may-not-be-coming-soon* 😉</sub>

All you gotta do is create your own or rename existing, removing the `.example` and edit the values. (Just be sure to make sure the permissions are correct!)

``` json title="default.json"
{
	"username": "admin",
	"password": "crafty"
}

```
