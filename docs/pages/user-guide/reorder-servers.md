# Re-ordering Servers :material-order-bool-descending:
!!! abstract "To re-order servers you must not be:"
    - Using search feature in the dashboard menu.<br>
    - Server re-ordering is only supported on desktop.
To change the order of your server click on the name of the server and drag it in the desired direction. Once you're hovering over where you would like to place it. Release your mouse button.

![Re-order example](../../img/page-assets/user-guide/server-reordering.gif)
<center><small>Your server is saved in this position automatically</small></center>
