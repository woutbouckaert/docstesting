# Crafty 4 Compatibility :material-test-tube:
  <a href="https://www.python.org" target="_blank">
    <img alt="Supported Python Versions" src="https://shields.io/badge/python-3.9%20%7C%203.10%20%7C%203.11%20-blue">
  </a>
  <a href="https://gitlab.com/crafty-controller/crafty-4/-/releases" target="_blank">
    <img alt="Crafty Version" src="https://img.shields.io/gitlab/v/tag/20430749?label=release" />
  </a>

With Crafty a being Python-based application it is designed to be compatible with a wide range of systems, giving users the flexibility to run it on various platforms. However, to ensure optimal performance and reliability, we have tested it's functionality on a specific set of distributions and can only provide support for those systems.

!!! success "Minecraft Version Compatibility"
    <center><bold>Crafty is currently tested on and works running MC Java versions 1.8 - latest<br>
    Versions older than 1.8 are not supported - run them at your own risk.</bold></center>

!!! note ""
    <center>**Please refer to the table below for a list of the distributions we have tested and confirmed compatibility with.**<br>
    We will not provide direct support to users deploying in untested environments.</center>

## Compatibility Chart / Support Matrix

| OS/Distro       |                 Versions                  | Support Level                                      |
| --------------: |:-----------------------------------------:| -------------------------------------------------- |
| Arch            |                  Latest                   | :material-check-all: Full                          |
| CentOS Stream	  |                   8, 9                    | :material-check-all: Full                          |
| Debian	        |                  11, 12	                  | :material-check-all: Full                          |
| Docker	        |                  Latest                   | :material-check: Arcadia Images Only               |
| Fedora	        |                  37, 38                   | :material-check-all: Full                          |
| Manjaro         |                  Latest                   | :material-check-all: Full                          |
| Mint            |                   21.2                    | :material-check-all: Full                          |
| OSX	            | 11 (Big Sur), 12 (Monterey), 13 (Ventura) | :material-check-all: Full                          |
| PopOS	          |                   22.04                   | :material-check-all: Full                          |
| Raspberry Pi OS |                  10, 11                   | :material-check-all: Full                          |
| Rocky Linux     |                    9.2                    | :material-check-all: Full                          |
| Windows         |                  10, 11                   | :material-check-all: Full                          |
| Ubuntu          |            22.04, 23.04, 23.10            | :material-check-all: Full                          |
| Unraid          |                  Latest                   | :material-check: Arcadia Image based Template Only |

!!! example "Request a missing distro"
    If you would like to see a new Linux distribution added to Crafty [please open an issue](https://gitlab.com/crafty-controller/crafty-installer-4.0/-/issues/new){:target="_blank"} on our installer repository.

    If you would like to see Crafty added to a new operating system [please open an issue](https://gitlab.com/crafty-controller/crafty-4/-/issues/new){:target="_blank"} on the Crafty 4 repo.
