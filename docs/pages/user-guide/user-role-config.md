# User/Role Configuration :fontawesome-regular-user:
The user management panel is a central component of Crafty, providing administrators, managers and users with a selection tools to manage user accounts and permissions within the Crafty.

This document serves as a guide to effectively navigate and utilize the user management panel, enabling you to effortlessly handle user creation, roles, and permissions.

## The User Management Panel
![Config cog arrow](../../img/page-assets/user-guide/settings-pointer.png){ align=right}

!!! abstract ""
    Getting to the **User/Role Manager** is incredibly easy all you have to do it click the cogs in the top right next to your profile picture.

![User/role manager overview](../../img/page-assets/user-guide/user-role-config/user-role-manager-overview.png)

Here we are able to Create new users and roles. Clicking the pencil icon :material-pencil: next to a user or role will allow you to edit their respective config settings.

## Adding a User
To create a user, Click `Add New User`

![User config overview](../../img/page-assets/user-guide/user-role-config/user-role-manager-user-config-overview.png){ align=left }

- Give the user a username.
- Give the user a password <br><small>(Password must be at least 6 characters, have a capital letter, a number and a symbol)</small>
- Repeat password
### Gravatar Option
Crafty now has Gravatar integration.<br> You may input your Gravatar email address to add your icon.<br>
<small>This email is strictly for Gravatar use only.<br> Arcadia technology does not have access to this email whatsoever. It is stored locally in your database.</small>
### User language
To change your user language select it from the dropdown. If the language is disabled, it is due to the fact that it is incomplete. If you would like to become a translator and translate Crafty [please open a ticket in our Discord.](../developer-guide/contributing/ask-a-question.md)

### Assign pre-existing role(s)
![User role assignment example](../../img/page-assets/user-guide/user-role-config/user-role-manager-user-config-roles.png)
### Crafty permissions
These variables limit the amount of resources that the user can create.

![Crafty Permissions User config overview](../../img/page-assets/user-guide/user-role-config/user-role-manager-user-config-perms.png)
<center>
Allow the user to create servers. <small>(If Quantity is -1 they can create an unlimited number of servers.)</small><br>
Allow the user to create a user account. <small>(If Quantity is -1 they can create an unlimited number of users.)</small><br>
Allow the user to create a role. <small>(If Quantity is -1 they can create an unlimited number of roles.)</small>
!!! example ""
    For example, if you want this user to only be able to create 3 servers allocate them the quantity of `3`

!!! danger "Superuser option"
    We **HIGHLY** recommend not creating additional super user accounts as they could in turn delete your user account and all servers.<br>
    **They have ACCESS TO EVERYTHING ON YOUR CRAFTY INSTALLATION.**<br>

    -----

    <small>The Super user option is only displayed if the user creating a new user account is a Super User.</small>
</center>
## Adding a Role
Roles allow you to segregate access to certain servers and related server management options.

To create a role, Click `Add New Role`. Adding a role gives you many options to fine tune access.
<center>
![Role config overview](../../img/page-assets/user-guide/user-role-config/user-role-manager-role-config.png)

Give your role a name, and select who you want to be able to manage this role (*if any*)<br>
<small>If you only want superusers to be able to manage this role leave manager unselected.</small>
</center>

Now add the servers you want to attach to this role, check off options to allow this role to see different parts of the server specified. See the table below for descriptions of each area:

| Permission | Description                                                                                                 |
| ---------: | ----------------------------------------------------------------------------------------------------------- |
| COMMANDS   | Allows role to start/stop/restart server                                                                    |
| TERMINAL   | Allows role to access the terminal                                                                          |
| LOGS       | Allows role to access server logs                                                                           |
| SCHEDULE   | Allows role to be able to create/delete/edit and access schedules                                           |
| BACKUP     | Allows role to be able to create/delete/download/restore and access backups                                 |
| FILES      | Allows role to access files. This includes uploading/downloading/deleting/unzipping/renaming/creating/etc   |
| CONFIG     | Allows role to access server config. As a basic user this only gives them access to minimal configurations  |
| PLAYERS    | Allows role to access player config. This allows them to OP, Kick, Ban, etc                                 |

