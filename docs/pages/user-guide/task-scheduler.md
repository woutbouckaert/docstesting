# Using the Task Scheduler :material-calendar-end-outline:

This document provides an overview of Crafty's task scheduler, which allows you to automate a wide range of tasks, such as backups, running commands on the server console, starting and stopping the server, and more.

With the task scheduler, you can set up complex schedules with ease, specifying intervals of days, months, and times, or using advanced cron expressions for more fine-grained control.

Additionally, you can chain tasks together so that they trigger automatically based on the success of it's parent task.

The task scheduler is a powerful tool to help you save time and streamline your workflow. This document covers essential topics such as scheduling tasks and configuring task chains, to help you get started and make the most of this feature.

## Scheduling Tasks
- Click "Create New Schedule"<br>
- Choose Whether you want this to be basic, cron, or chain reaction.<br>
    - **Basic** - Crafty will break down all the scheduling triggers for you.<br>
    - **Cron** - You will manually use a cron string to set the trigger.<br>
    - **Trigger** - This schedule will run after its linked schedule runs.

<small>Options will appear and disappear depending upon user's configuration options</small>

=== "Basic :material-update:"
    !!! example ""
        ![sheduler basic example](../../img/page-assets/user-guide/task-scheduler/schedules-basic.png)
        **Action** - What task do you want your schedule to complete?<br>
        **Interval** - How often would you like this schedule to trigger? (This will take an integer)<br>
        **Delete After Execution** - This would only be triggered one time. Crafty would delete the schedule after execution when this is checked.

=== "Cron <small>:fontawesome-solid-asterisk::fontawesome-solid-asterisk::fontawesome-solid-asterisk::fontawesome-solid-asterisk::fontawesome-solid-asterisk:</small>"
    !!! example ""
        ![sheduler cron example](../../img/page-assets/user-guide/task-scheduler/schedules-cron.png)
        **Cron String** - This is a validated cron string. For more information on cron please visit [Crontab in linux w/ examples](https://www.geeksforgeeks.org/crontab-in-linux-with-examples/){:target="_blank"} <br>
        For help creating cron strings we recommend [crontab.guru](https://crontab.guru/){:target="_blank"}.
        ??? note "Non-standard weekday index"
            Due to the way our scheduling module works the last value in the Cron string that determines the day is offset by one... So `0` is **Monday** and not **Sunday**<br><br>
            {>>HINT: Using day abbreviations still works just fine (MON-SUN).<<}
        **Delete After Execution** - This would only be triggered one time. Crafty would delete the schedule after execution when this is checked.

=== "Chain Reaction :material-link:"
    !!! example ""
        ![sheduler chain reaction example](../../img/page-assets/user-guide/task-scheduler/schedules-reaction.png)
        **Action** - What task do you want your schedule to complete?<br>
        **Delay Offset** - How long Crafty will wait after dispatching the parent schedule before firing this one.<br>
        **Select a Parent Schedule** - The schedule that will trigger this schedule upon it's completion.<br>
        **Delete After Execution** - This would only be triggered one time. Crafty would delete the schedule after execution when this is checked.

## Example Schedules
Here are some example schedules you could run:
![Example Schedules](../../img/page-assets/user-guide/task-scheduler/schedules-example-list.png)
