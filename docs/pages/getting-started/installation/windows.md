# Windows Installation Guide :simple-windows:

## Crafty Portable :fontawesome-solid-cart-flatbed-suitcase:
### Running Crafty Portable
1.  Download the most recent Windows build from
    [https://craftycontrol.com](https://craftycontrol.com/#download){:target="_blank"}
2.  Unzip the file downloaded
3.  Double click on Crafty.exe

### Updating Crafty Portable
!!! warning "Make sure to stop Crafty before updating (++control+c++)"
The Windows Portable build comes with an updater packaged with it, simple as launching the updater and following the prompts 🚀

## Crafty Windows Service :material-server: <sub><small>beta</small></sub>
> A .Net6 based Windows Service to run Crafty as a service on Windows

### Contents of the package
| File              | Description                                                   |
| ----------------- | ------------------------------------------------------------- |
| IntallService.ps1 | `PowerShell` Script used to Install the Service on the computer |
| UninstallService.ps1 | `PowerShell` Script used to Uninstall the service on the computer |
| appsettings.json | Configuration file to modifiy some services settings (Installation Folder, Auto-Update, ...) |
| CraftyService.exe | The executable of the service (The one that does all the work) |

### Install Steps
=== "Windows 10/11 :fontawesome-solid-computer:"
    !!! example ""
        **Requirements:**

        - Windows 10 or 11
        - [.NET 6](https://dotnet.microsoft.com/download/dotnet/6.0){:target="_blank"} or Higher
        - Internet Connection to download Crafty Files

        -----

        **Preparing with Installation Script:**

        1. Download the latest Windows Service Package: [Crafty 4 - Windows Service](https://gitlab.com/crafty-controller/crafty-4-windows-service/-/jobs/artifacts/master/download?job=win-build)
        2. Start the `PowerShell` console with administrator privileges.
        3. Run the `InstallService.ps1` script.
        4. Firstly, it will create a user to run the service and asks for a password for this user. (Remember this for later)
        5. Then, it will asks if you want to start the service at Startup.
        6. And that's it for the Installation Script.
        7. The `PowerShell` console can be closed.

        ??? warning "What to do if a login failure appears?"
            <center>During the installation if you recieve "Error 1069: The Service did not start due to login failure."
            ![Service login failure example](../../../img/page-assets/getting-started/installation/windows/Crafty_ServiceCantStart.png)</center>

            1. Open the **Services Manager**.
            2. Go to the Crafty Service properties.
            3. In the connection tab, check if Crafty User is used for opening a session, if not fill in the crafty user credentials.
            <figure markdown>
                ![Image title](../../../img/page-assets/getting-started/installation/windows/Crafty_userLogonOK.png){ width="300" }
                <figcaption>*(you need to fill again the password you set earlier)*</figcaption>
            </figure>
            <figure markdown>
                ![Image title](../../../img/page-assets/getting-started/installation/windows/Crafty_serviceLogonOK.png)
                <figcaption>*(You should recieve this messasge if successful)*</figcaption>
            </figure>
            4. You can now start the Crafty Service 🎉

        -----

        **Now Crafty Service is ready to run:**

        1. Go in the **Services manager**
        2. Then you can Start the **Crafty Service**

=== "Windows Server :fontawesome-solid-server:"
    !!! example ""
        **Requirements:**

        - Windows 2016 or Higher
        - [.NET 6](https://dotnet.microsoft.com/download/dotnet/6.0){:target="_blank"} or Higher
        - Internet Connection to download Crafty Files

        -----

        **Preparing with Installation Script:**

        1. Download the latest Windows Service Package: [Crafty 4 - Windows Service](https://gitlab.com/crafty-controller/crafty-4-windows-service/-/jobs/artifacts/master/download?job=win-build)
        2. Start the `PowerShell` console with administrator privileges
        3. Run the `InstallService.ps1` script
        4. Firstly, it will create a user to run the service and asks for a password for this user. (Remember this for later)
        5. Then, it will asks if you want to start the service at Startup
        6. And thats' it for the Installation Script
        7. The `PowerShell` console can be closed

        ??? warning "What to do if a login failure appears?"
            <center>During the installation if you recieve "Error 1069: The Service did not start due to login failure."
            ![Service login failure example](../../../img/page-assets/getting-started/installation/windows/Crafty_ServiceCantStart.png)</center>

            1. Open the **Services Manager**.
            2. Go to the Crafty Service properties.
            3. In the connection tab, check if Crafty User is used for opening a session, if not fill in the crafty user credentials.
            <figure markdown>
                ![Image title](../../../img/page-assets/getting-started/installation/windows/Crafty_userLogonOK.png){ width="300" }
                <figcaption>*(you need to fill again the password you set earlier)*</figcaption>
            </figure>
            <figure markdown>
                ![Image title](../../../img/page-assets/getting-started/installation/windows/Crafty_serviceLogonOK.png)
                <figcaption>*(You should recieve this messasge if successful)*</figcaption>
            </figure>
            4. Proceed to policy setup ➡️

        -----

        **We need to grant the CraftyUser to run services in the Windows Group Policy:**
        > This step is not needed if the prior step gives you a dialog box of adding the CraftyUser to it.

        1. Open the **Group Policy Strategy Editor**
        2. Go in **Computer Config**
        3. Then in **Windows Settings**
        4. Then in **Security Settings**
        5. Then in **Local Strategies**
        6. Then in **User rights attribution**
        7. You need to add the Crafty User at the `Open a session as a service`

        -----

        **Now Crafty Service is ready to run:**

        1. Go in the **Services manager**
        2. Then you can Start the **Crafty Service**

### Configuring the Service
!!! example ""
    In the Folder you have a configuration file named `appsettings.json`
    The part you want to use is this one :
    ```json
    "Crafty": {
        "ServiceConfig": {
        "DefaultGitPath": "C:\\Program Files\\Git\\bin",
        "DefaultPythonPath": "C:\\Program Files\\Python3",
        "CraftyDirectory": "crafty-4",
        "AutoUpdate": true
        },
        "UserConfig": {
        "CraftyBranch":  "master"
        }
    }
    ```

    -----
    !!! note "Updating Defaults"
        - If you installed Git in a different folder that the default one, you can set its path in the `DefaultGitPath` field.
        - If you installed Python in a different folder or have a different vertsion that the default one, you can set its path in the `DefaultPythonPath` field.
        - If you want to place Crafty in a different Directory that the Service Files, you can set its path in the `CraftyDirectory` field. **!! Not recommended due to Access Rights !!**
        - You can configure if you want Crafty to be updated at the Service Startup in `AutoUpdate` field.
        - If you want to be with a specific branch of Crafty, you can set it in the `CraftyBranch` field. *(Works only with master or dev branch for now.)*

### Updating the Service
!!! example ""
    **Requirements:**

    - Rights on the folder containing the Crafty Service Files
    - Download of the latests release of the Crafty Service

    **Update Steps:**

    1. Stop the Crafty Service in the Service Manager
    2. Copy/Paste, the new files in place of the old ones
    3. Start again the Crafty Service in the Service Manager

### Uninstall Steps
!!! example ""
    !!! warning "Don't forget to save your Minecraft Worlds before uninstalling!"

    **Uninstallation Steps:**

    1. Start the `PowerShell` console with administrator privileges
    2. Run the `UninstallService.ps1` script
    3. Firstly, it will remove the service from windows.
    4. Then, it will asks if you want to remove the CraftyUser
    5. And that's it for the Uninstallation Script
    6. The `PowerShell` console can be closed
    7. You can now delete your server files.


-----

## Post-Install
<center>
Excellent, by this point you should have successfully stood up your crafty instance!<br>
Great work, You're ready to proceed to the next step 🎉<br>
[How to access the dashboard :material-application-import:](../access.md){ .md-button }
</center>

