# macOS Installation Guide :simple-apple:
!!! abstract "Crafty Requirements"
    -   You will need brew.sh installed in your mac. Get it from
        [https://brew.sh](https://brew.sh){:target="_blank"}
    -   This guide assumes you have a decent understanding of the macOS
        Terminal environment and are comfortable with the command line.
    -   This guide also assumes you will be installing Crafty into `/var/opt/minecraft/crafty`.
    -   We also assume your server will be at `/var/opt/minecraft/server`.
    -   We also assume the user account running crafty will have full
        read/write/execute permissions on these folders.
    -   Finally this guide assumes you have the following software installed and up to date: **Git, Python 3.9+**.

    NOTE: You will need to install **Java** before starting any **Minecraft** servers.

### Installation

#### Install host dependancies

##### Brew.sh

macOS lacks a proper package manager and in order to install a couple of the required software via the command line you will need Brew.sh.

Go to your `/Applications/Utilities` and launch **Terminal.app**
<br>- or -<br>
Press ++command+space++ and type **Terminal.app** and hit ++enter++

``` shell title="Use the following Command:"
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)";
```

{==

The command above runs and you will see the installation progress. It will inform you that the ***Command Line Tools for Xcode*** will get installed.

==}

Towards the end you will be asked to enter your {++account password++} then the installation will continue.

You will be notified when Brew begins downloading the ***Command Line Tools for Xcode*** and depending on your connection the download will take about 5 minutes. Once the tools have downloaded, they will install on your mac and the installation of Brew will be completed. 🎉

!!! note "Adding brew to path!"
    Once the download and install are complete brew will list 3 commands you need to enter to add homebrew to `PATH` and `MANPATH`.

    Those commands will look something like this:

    ``` shell
    ==> Next steps:
    - Run these three commands in your terminal to add Homebrew to your PATH:
        echo '# Set PATH, MANPATH, etc., for Homebrew.' >> /Users/crafty/.zprofile
        echo 'eval "$(/opt/homebrew/bin/brew shellenv)"' >> /Users/crafty/.zprofile
        eval "$(/opt/homebrew/bin/brew shellenv)"
    ```
    ⚠️ If you do not complete those steps the rest of this install with be **PAINFUL** ⚠️

-----

##### Git, Java and Python 3.9+

``` shell title="Install Git and Python 3.9+"
brew install git python3
```
``` shell title="Install OpenJDK (cask)"
brew install --cask homebrew/cask-versions/adoptopenjdk8 # (1)!
```

1. `brew cask` is an extension to brew that allows management of graphical applications through the [Cask](https://github.com/Homebrew/homebrew-cask){:target="_blank"} project. <br>`--cask` offers a way to command line manage the installation of graphical applications.

``` shell title="Upgrade pip"
pip install --upgrade pip
```

-----

#### Installing Crafty

##### Create a directory for Crafty
``` shell title="Let's make crafty a place to live on your server."
sudo mkdir -p /var/opt/minecraft/crafty /var/opt/minecraft/server
```
``` shell title="Make the directories owned by the 'crafty' service user"
sudo chown -R <your_username>:admin /var/opt/minecraft #(1)!
```

1.  **For Example** If your username on your mac is **Totoro** then the
command will be:<br>`sudo chown -R totoro:admin /var/opt/minecraft/crafty`

``` shell title="Once created lets change our working directory to our new installation dir"
cd /var/opt/minecraft/crafty
```

##### Clone the Crafty Repository

*Please be sure to be in `/var/opt/minecraft/crafty` folder before cloning the repo.*<br>
{>>To check type 'pwd' and make sure it says '/var/opt/minecraft/crafty' before you continue.<<}

``` shell title="Clone Repo"
git clone https://gitlab.com/crafty-controller/crafty-4.git
```

##### Create a Virtual Environment
``` shell title="Make venv called '.venv'"
python3 -m venv .venv # (1)!
```


1.  We prefix a `.` to the name of the `virtual environment` because in a Linux/Unix context it makes that the file or directory hidden, you cannot see it in the file explorer (`ls`) unless the `-a` flag is provided.


##### Activate the Virtual Environment
``` shell
source .venv/bin/activate # (1)!
```

1.  You know when you're in your virtual environment because your prompt will change to include `(venv)`

##### Install Crafty's package dependancies
``` shell title="(venv) Change to the crafty-4 dir that was cloned down"
cd /var/opt/minecraft/crafty/crafty-4
```
``` shell title="(venv) Install Crafty's required packages"
pip3 install --no-cache-dir -r requirements.txt
```

### Run Crafty

Make sure you're still in your `venv` and in the `/var/opt/minecraft/crafty/crafty-4` directory.<br>
{>>To check type 'pwd' and make sure it says '/var/opt/minecraft/crafty/crafty-4' before you continue.<<}
``` shell title="(venv) Run Crafty inside the virtal environment"
python3 main.py
```
### Updating Crafty
!!! warning "Make sure to stop Crafty before updating"
    In your Terminal window press ++control+c++ to stop Crafty

!!! info "If you experience git telling you to commit or stash local changes and you have not made any changes"
     In the `/var/opt/minecraft/crafty/crafty-4` directory, run the following:
     ``` shell
     git reset --hard origin/master
     ```
     If you have made changes you might want to stash them, but popping them could cause adverse effects.<br> {>>Make sure to run the the following update commands after running this.<<}

``` shell
cd /var/opt/minecraft/crafty/crafty-4
```
``` shell
git pull
```
``` shell
cd ..
```
``` shell
source .venv/bin/activate
```
``` shell
cd crafty-4
```
``` shell
pip3 install -r requirements.txt --no-cache-dir
```

-----

### Post-Install
<center>
Excellent, by this point you should have successfully stood up your crafty instance!<br>
Great work, You're ready to proceed to the next step 🎉<br>
[How to access the dashboard :material-application-import:](../access.md){ .md-button }
</center>
