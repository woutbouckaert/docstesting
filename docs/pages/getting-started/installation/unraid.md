# Unraid Docker Template :simple-unraid:
!!! bug "Support for third-party UnRAID images"
    As with any software, image variants built by third-parties come in many shapes and sizes.

    Please be advised if you are using an unraid template that does not use our __**official image**__ we will not be able to provide you support. We can only support Arcadia images where we are familiar with the environment.

##  Install from Community App Marketplace
For a easy installation it is recommended to use Community Applications
to install software like crafty on UnRaid.

1.  Go to Apps and search for "Crafty"
2.  Select Crafty-4
3.  Install the docker template to your unraid server


## Path and port mapping {#path_and_port_mapping}

### Volume Mounts
When referring to file system locations in a containered Crafty instance, don't use host pathing as that wont exist in the container, please refer to the corresponding container location below:

| Unraid location                    | Container location | Usage                                                            |
| :--------------------------------- | :----------------- | :--------------------------------------------------------------- |
| /mnt/user/appdata/crafty-4/backups | /crafty/backups    | Storage for Server backups                                       |
| /mnt/user/appdata/crafty-4/logs    | /crafty/logs       | Storage for Crafty logs                                          |
| /mnt/user/appdata/crafty-4/servers | /crafty/servers    | Storage for Imported/Built Servers                               |
| /mnt/user/appdata/crafty-4/config  | /crafty/app/config | Storage for Crafty's config and database files                   |
| /mnt/user/appdata/crafty-4/imports | /crafty/import     | Where you will drop server zips/directories for crafty to import |

### Ports

Crafty will require various ports to be exposed to be able to be accessible.

The following is a list of the ports that are required:

  | Port          | Requirement                                 |
  | ------------- | ------------------------------------------- |
  | 8443          | Required for Panel Access                   |
  | 8123          | Required for Dynmap(Optional)               |
  | 19132/udp     | Required for Bedrock Minecraft Server       |
  | 25500-25600   | Required Port range for Minecraft servers   |

- Ports for Minecraft servers to connect to (should always be 100 in total, because the amount of ports used by docker containers can not be dynamically adjusted)

- Dynmap Port conflicts [8123]<br>
{>>This is frequently conflicting with Home Assistant, it is recommended to be changed only if needed<<}

-----

## Post-Install
<center>
Excellent, by this point you should have successfully stood up your crafty instance!<br>
Great work, You're ready to proceed to the next step 🎉<br>
[How to access the dashboard :material-application-import:](../access.md){ .md-button }
</center>
