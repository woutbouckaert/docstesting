# Reporting Docs Issues and Inconsistencies :material-file-document:

We strive to provide accurate and up-to-date documentation for Crafty, but we understand that errors and inconsistencies can sometimes occur.

If you come across any issues or inconsistencies while using our documentation, we encourage you to report them. Your feedback helps us improve the quality and usability of our documentation.

To report documentation issues or inconsistencies, please visit our [Crafty Documentation Repository](https://gitlab.com/crafty-controller/crafty-documentation/-/issues/new) and create a new issue. We appreciate detailed descriptions of the problem you encountered and any suggestions for improvement.

While creating the issue, please include the following information:
!!! example ""
    - **Page/Section**: Specify the specific page or section where you found the issue.
    - **Description**: Clearly describe the problem or inconsistency you encountered.
    - **Expected Information**: Provide details about what you expected to find or how the documentation could be improved.
    - **Priority/Severity**: Indicate the priority and severity of the issue based on its impact.
        - [ ] **High Priority/Severity:** Documentation issues that significantly impact understanding or usage, such as critical inaccuracies, misleading instructions, or missing essential information.
        - [ ] **Medium Priority/Severity:** Documentation issues that affect usability or comprehension, but with less immediate impact, such as minor inaccuracies, unclear explanations, or inconsistent terminology.
        - [ ] **Low Priority/Severity:** Documentation issues that are cosmetic or have minimal impact on understanding or usage, such as minor typos, formatting inconsistencies, or suggestions for minor improvements.
<center>
{>>An issue template will be made soon<<} <!-- -TODO Make issue templates -->
</center>

By reporting documentation issues, you contribute to enhancing the overall experience for our community.

Thank you for taking the time to make Crafty's documentation more accurate and user-friendly,<br>
The Crafty Team 💖

