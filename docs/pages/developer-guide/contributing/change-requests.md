# Submitting a Feature/Change Request :material-lightbulb-on:

We value your feedback and ideas for improving Crafty. If you have a feature request or a change request, we encourage you to submit it through our Crafty GitLab repository. By doing so, you contribute to shaping the future of our platform.

To submit a feature request or change request, please follow these steps:

1. Visit our [Crafty GitLab repository](https://gitlab.com/crafty-controller/crafty-4/-/issues/new){:target="_blank"} and create a new issue.
2. Select the **appropriate issue template** based on your request:
=== "Feature Request"
!!! example ""
    **Problem Statement**
        *What is the issue being faced and needs addressing?*

    **Who Will Benefit?**
        *Will this fix a problem that only one user has, or will it benefit a lot of people?*

    **Benefits and Risks**
    What benefits does this bring?

    - reduced support issues
    - extended functionality

    **What risks might this introduce?**

    - May result in security issues
    - May break current application flow
    - License issues

    **Proposed Solution**
        *How would you like to see this issue resolved?*

    **Examples**
        *Are there any examples of this existing in other software?*

    **Priority/Severity**

    - [ ] High (This will bring a huge increase in performance/productivity/usability)
    - [ ] Medium (This will bring a good increase in performance/productivity/usability)
    - [ ] Low (anything else e.g., trivial, minor improvements)

=== "Change Request"
!!! example ""
    **Summary**
        *Outline the issue being faced and explain why this change is necessary.*

    **Area of the System**
        *Specify the affected area(s) of the system (e.g., Login/Dashboard/Terminal/Config).*

    **How does this currently work?**
        *Describe how the system currently works.*

    **What is the desired way of working?**
        *After the change, what should the process/operation be?*

    **Priority/Severity**

    - [ ] High (This will bring a huge increase in performance/productivity/usability)
    - [ ] Medium (This will bring a good increase in performance/productivity/usability)
    - [ ] Low (anything else e.g., trivial, minor improvements)

Once you have chosen the appropriate template and filled in the requested information, submit the issue. Our team will review your request and consider it for future development.

We appreciate your valuable input and thank you for helping us make Crafty even better!

Thank you for your support,<br>
The Crafty Team 💖

