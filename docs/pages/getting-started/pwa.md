# Crafty Controller Mobile App :octicons-device-mobile-24: <sub><small>beta</small></sub>
The mobile app is a Progressive Web App (PWA). This means that it is a website run inside its own container on your phone.

The installation will be slightly different based on your phone OS and the browser you use. These docs have instructions for Safari on iOS and Chrome on Android, though they should be able to be applied to most browsers.

!!! note "Requirements & Limitations"
    - Please note, that you MUST have a 'CA Verifiable certificate' or most features of the PWA will not work.<br>
        *Let's Encrypt is a free, automated, and open certificate authority (CA) that gives digital certificates in order to enable HTTPS (SSL/TLS) for websites. Learn more about Let's Encrypt configuration by following the [Proxies Doc](./proxies.md)*
    - The mobile app will only be functional if your Crafty instance is accessible. In order to access it outside of your house you may need to port forward and use a reverse proxy.

## Installing the Crafty Controller Mobile App <sub><small>[:octicons-tag-24: 4.1.0][PWA support]{:target="_blank"}</small></sub>

=== "Safari :material-apple-safari:"
    1.  Open your Crafty Instance in Safari and select the share button.
    2.  Scroll down and select "Add to Home Screen"
    3.  Select "Add" in the top right corner.

=== "Chrome :material-google-chrome:"
    1. Open your Crafty Instance in Chrome and open the more menu in the top right corner.
    2. Select "Add to home screen/install app"
    3. Select "Add"
    4. Select "Add to Home Screen"

[PWA support]: https://gitlab.com/crafty-controller/crafty-4/-/releases/v4.1.0

-----

## Troubleshooting
- Make sure that you are using HTTPS.
- Make sure you are using a signed SSL certificate.
- Make sure that you have updated Crafty.
