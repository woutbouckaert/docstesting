# Troubleshooting / FAQ :material-frequently-asked-questions:
Welcome to our Frequently Asked Questions (FAQ) page, where you'll find answers to common queries and handy solutions to the most frequently encountered issues.
!!! question ""
    This compilation brings together the most commonly asked questions from our Discord community and GitLab issues.

    Before reaching out for support, we recommend using the search feature on our documentation platform to find answers online. Take a moment to review this document, as it may already contain the solution you're looking for.

    The FAQ page is regularly updated to provide the latest insights and solutions. It's a resource designed to help you quickly resolve your concerns without the need to contact support.
<center>
**Happy troubleshooting!** 🥳
</center>
-----

!!! quote ""
    ### Setting up Minecraft Forge Servers with Crafty (1.18 or 1.19)

    -----

    !!! success "Forge via the Server Builder"
        We recommend using the server builder to create a Forge server, It automates all the manual steps making it a quick and streamlined process.<br><br>
            - 1. Create server using Crafty's server creation tool with desired versioning.<br>
            - 2. Replace `nogui`with the `--installServer` flag to the arguments in your startup string.<br>
            - 3. Click the start button on Crafty. This will start the forge installer.<br><br>
        Continue with **Step 3** below.

    Setting up Forge servers with Crafty can be a little tricky, especially with newer versions that require an installer. Follow the steps below to ensure a smooth setup process:

    1. **Prepare the Forge Server:**
        - Run the Forge Installer and choose a directory (e.g., `/forge`) to extract the server files.
        - Once extracted, compress the `/forge` directory into a zip file called `forge.zip`.

    2. **Import the Forge Server into Crafty:**
        - In Crafty's control panel, click on 'Create a Server' and follow the instructions to import the Forge server.
        - Provide the necessary details during the import process, including the server name, server path (location of the `forge.zip` file), root directory, server executable file, minimum and maximum memory settings, and server port.

    3. **Update the Server Execution Command:**
        - Open the `.bat` or `.sh` file created by the Forge installer and review its contents.
        - Modify your Server Execution Command in Crafty by adapting the command structure from the installer file:
            - **For Windows**: Replace the existing command with the following:
            ```
            java -Xms4000M -Xmx4000M @libraries\net\minecraftforge\forge\[your-forge-version-do-not-copy]\win_args.txt nogui
            ```
            - **For Linux/MacOS**: Replace the existing command with the following:
            ```
            java -Xms4000M -Xmx4000M @libraries/net/minecraftforge/forge/[your-forge-version-do-not-copy]/unix_args.txt nogui
            ```
        - Ensure that your modified command aligns with the structure and parameters of the installer file, excluding any unnecessary elements such as `%*` or `$@`.

    4. **Start the Forge Server:**
        - Save the changes in Crafty and start the server.
        - Keep in mind that Forge servers may take some time to fully initialize, so please be patient as it loads the necessary resources and displays the information on the terminal.

    > **Note:** Be cautious when modifying the Server Execution Command and ensure that you refer to the specific `.bat` or `.sh` file provided by the installer to extract the correct information.

    By following these steps, you should be able to set up your Minecraft Forge server successfully within Crafty. If you encounter any issues or need further assistance, don't hesitate to reach out to our support team. Happy crafting!

!!! quote ""
    ### Setting up Modrinth modpacks with Crafty

    -----
    !!! note "You can't create Modrinth modpacks directly inside Crafty."
        Modrinth modpacks differ from curseforge, they come bundled into a ``.mrpack`` format, which needs to be unpacked!
        Because of this, you will need to prepare them outside of Crafty and import them.

    Modrinth do not provide any official tool for server modpacks.

    This tutorial will utilize the third-party tool [nothub/mrpack-install](https://github.com/nothub/mrpack-install/){:target="_blank"} on your local system (Not the one running Crafty).


    === "Windows :simple-windows:"
        1. [**Download the latest release of 'mrpack'**](https://github.com/nothub/mrpack-install/releases/latest/download/mrpack-install-windows.exe){:target="_blank"}
        2. **Open command prompt and navigate to where mrpack downloaded**
            1. Type CMD into the search bar and use `cd Downloads` to navigate into your downloads directory.
        3. **Tool usage**
            1. **In command prompt, check connectivity with `mrpack-install-windows.exe ping`**<br>
            > If you get "**404**" error, please check if `https://api.modrinth.com` responds in your browser with basic JSON data.
            2. **To install a modpack use `mrpack-install-windows.exe <modpack-name>`**<br>
            > If a modpack name includes a space, look at the URL, most likely the space will be replaced with a dash `-`
            3. **A new folder called `mc` should have been created.**<br>
            > In that folder, you will find your downloaded modpack

    === "Linux :simple-linux:"
        1. **Download the latest release of 'mrpack'**
        ``` shell
        curl -LO https://github.com/nothub/mrpack-install/releases/latest/download/mrpack-install-linux
        ```
        2. **Make the modifier executable**
        ``` shell
        chmod +x ./mrpack-install-linux
        ```
        3. **Tool usage**
            1. **In command prompt, check connectivity with `./mrpack-install-linux ping`**<br>
            > If you get "**404**" error, please check if `https://api.modrinth.com` responds in your browser with basic JSON data.
            2. **To install a modpack use `./mrpack-install-linux <modpack-name>`**<br>
            > If a modpack name includes a space, look at the URL, most likely the space will be replaced with a dash `-`
            3. **A new directory called `mc` should have been created.**<br>
            > In that directory, you will find your downloaded modpack

        ``` shell title="One line download and connectivity check"
        curl -LO https://github.com/nothub/mrpack-install/releases/latest/download/mrpack-install-linux && chmod +x mrpack-install-linux && clear && ./mrpack-install-linux ping
        ```
    -----
    4.**Importing the modpack into crafty**

    Once you have prepared the above, make a zip archive of downloaded files, and use Crafty's [Zip Import](./server-creation/minecraft.md#import-a-server) function to create your server.

    When selecting your 'Root Dir' make sure to exclude the `mc` dir, structure of your server should look like this:

        Files
        ├── world
        │   └─── (world files)
        └── mods
    And **not** like this, if you see `mc`, set that as the root dir.

        Files
        └── mc
            ├── world
            │
            └── mods

!!! quote ""
    ### ModuleNotFoundError: No module named 'zoneinfo'

    -----

    As of [:octicons-tag-24: 4.0.21][v4.0.21]{:target="_blank"}, Crafty's minimum supported python version is **3.9**. ([Crafty Compatibility Chart](../getting-started/compatibility.md))

    If you're feeling adventurous you can follow the below steps to upgrade to **python 3.9** and re-create your `venv`.

    If you also need to keep **3.8** on your machine for any reason, you can create the `venv` with **3.9** using following commands (Remember you will need `python-virtualenv` installed):
    ``` bash
    cd /var/opt/minecraft/crafty
    rm -rf .venv
    virtualenv --python="/usr/bin/python3.9" ".venv" # (or whichever version you're running)
    source .venv/bin/activate
    cd crafty-4
    pip3 install -r requirements.txt --no-cache-dir
    ```

    If you need help updating to python 3.9 our staff is always willing to help with these types of things.

!!! quote ""
    ### CRITICAL: Root detected. Root/Admin access denied.

    -----

    Crafty has a built in mechanism to prevent users from running the application in an elevated environment.<br>
    You are either trying to run the application with `sudo`, as the root user or as a local administrator on Windows Server.

    To resolve this:<br>
    - **On Linux:** Please run Crafty with the `crafty` service user that was created during the install.<br>
    *(To switch to this user enter: `sudo su - crafty`, Don't forget to enter your virtual environment too)*<br>
    - **On Windows Server:** Please create a service account which does not have local administrator privileges by default, and run crafty with this user.

    ??? question "Why is this root check thing and why can't I bypass it?"
        Running a public-facing web application with root privileges can pose significant security risks.

        Granting root access to the web application means that if a vulnerability is exploited, an attacker could gain complete control over the entire system. (Even in docker this can lead to container breakout)

        This puts sensitive data, user accounts, and the overall server security at great risk. It is highly recommended to follow security best practices, such as running the web application with limited privileges and employing strong access controls, to minimize the potential impact of security breaches and protect both your application and the underlying system.

!!! quote ""
    ### Can I use scp, FTP or SFTP?

    -----

    This is something that you *can do*... but you will have a great difficulty maintaining this method of transferring files to your Crafty instance, As all of the above methods would require additional steps to ensure correct **file ownership** and **permission masks** are applied to the transferred files.

    If you choose to go the route of using one of these tools to transfer files to your crafty directories, {==we will not be able to provide support.==}

    !!! success ""
        **We recommend using craftys built in [Server File Manager](./file-manager.md) to upload, download and manage files on your deployed dedicated server apps.**

!!! quote ""
    ### How do I give my friends access to Crafty?

    -----

    There are multiple ways where you can provide your friends access to your Crafty dashboard,

    The first more common choice of providing access is by opening ports on your router to use with a reverse proxy.<br>
    [You can find documentation here](../getting-started/proxies.md) on how to do that.

    The second choice, If you're not comfortable punching a hole in your Firewall and having Crafty accessible to the open Internet, You can look at using a VPN, such as [Tailscale (Easy)](https://tailscale.com){:target="_blank"} or [Wireguard (Advanced)](https://www.youtube.com/watch?v=5NJ6V8i1Xd8){:target="_blank"}.

    This will essentially allow your friends to access crafty as you would normally on your local network, without crafty being exposed to the public internet.

!!! quote ""
    ### RuntimeError: can't start new thread

    -----

    When running crafty and docker you may come across the following funky runtime error.     This issue is only prevalent on old versions of docker.

    ``` py
    Traceback (most recent call last):
    File "/crafty/main.py", line 157, in
    controller = Controller(database, helper, file_helper, import_helper)
    File "/crafty/app/classes/shared/main_controller.py", line 78, in init
    self.support_scheduler.start()
    File "/crafty/.venv/lib/python3.10/site-packages/apscheduler/schedulers/background.py", line 38, in start
    self._thread.start()
    File "/usr/lib/python3.10/threading.py", line 935, in start
    _start_new_thread(self._bootstrap, ())
    RuntimeError: can't start new thread
    ```
    <div class="result" markdown>

    **Solution:** The recommended solution is to upgrade your local Docker version to greater than 20.10.10 by following the [Docker install instructions](https://docs.docker.com/engine/install/ubuntu/){:target="_blank"} (or any other appropriate method).<br>
    *-Bare in mind you may need to update your Linux distribution if you are on an end-of-life release, As docker may have stopped packaging new versions for that distribution versdion.-*

    **Why?:**<br>
    > This is because the default seccomp profile of Docker 20.10.9 is not adjusted to support the clone() syscall wrapper of glibc 2.34 adopted in Ubuntu 21.10 and Fedora 35.

    *Source: [ubuntu:21.10 and fedora:35 do not work on the latest Docker (20.10.9)](https://medium.com/nttlabs/ubuntu-21-10-and-fedora-35-do-not-work-on-docker-20-10-9-1cd439d9921){:target="_blank"}*
    </div>

!!! quote ""
    ### Your connection isn't private

    -----

    You may recieve a warning screen in your browser notifying "Your connection is not private", **don't worry it is!**<br> Traffic between the dashboard and your browser is still encrypted.

    It's just because crafty by default uses a [self-signed certificate](https://sectigo.com/resource-library/what-is-a-self-signed-certificate){:target="_blank"}, which your browser can't verify it's authenticity against the big certificate authorities.

    **Firefox:** "Warning: Potential Security Risk Ahead" `MOZILLA_PKIX_ERROR_SELF_SIGNED_CERT`<br>
    **Chrome/Chromium:** "Your connection isn't private" `NET::ERR_CERT_AUTHORITY_INVALID`

    In these instances you can click the dropdown or 'Advanced...' button, and proceed anyway.

    You can also add crafty's self-signed certificate as an exeption to your browser<br>
    or<br>
    You can use a generally trusted certificate. a good point to start would be [Let’s Encrypt](https://letsencrypt.org){:target="_blank"}

[v4.0.21]: https://gitlab.com/crafty-controller/crafty-4/-/blob/master/CHANGELOG.md#-4021-20230304
