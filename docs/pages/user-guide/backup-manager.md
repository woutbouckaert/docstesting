# Using the Backup Manager :material-backup-restore:

This document provides an overview of Crafty's backup manager. The backup manager is a powerful tool that allows you to create and manage backups of your servers with ease. You can specify the max number of backups to store, define wither the server should be shut down while backing up, and choose where to store your backups.

Moreover, you can specify what data to backup, including files, folders, ensuring that your minecraft worlds always protected. The backup manager is an essential feature to help you safeguard your data and recover from unexpected events.

This document covers essential topics such as configuring backup settings, scheduling backups and restoring data from backups, to help you get started and make the most of this feature."
## Configuring the backup manager
![Backup Manager Overview](../../img/page-assets/user-guide/backup-manager/backup-manager-overview.png)
### Storage Location
Pretty self explanitory, this is where your backups are stored, standard users will not have the option to change this but 'Super Users' are able to set the backup path. You may choose a custom path.

### Max Backups
This option sets the number of backups that are retained, garbage collecting old backups past the defined number you would like Crafty to keep. <small>(Set `0` for no limit)</small>

### Compress Backup
Choose whether or not you would like Crafty to compress your backups for you.
??? warning "Chunk artifacts / corruption"
    We did not believe backup compression should be a feature as compressing chunk data can lead to unintended side effects such as chunk corruption or artifacting. But you guys really wanted it so we provided 😉 Use it at your own risk!

### Shutdown During Backup
This option allows you to shutdown the server for the duration of the backup, starting your server again after the backup has completed.
!!! question ""
    This is recommended as unlike other backup methods, we create our backups via copying file system data, Which means that if changes are actively being made on your server, this could lead to data loss or corruption.

### Run command before/after backups
This allows you to run server specific commands before and/or after a backup takes place.

It can be useful if you want to perform certain tasks before the server shuts down for a backup, for example announcing to players a backup is commencing or if you would like to place this server in maintenance mode, kicking players for the duration of the backup.

### Backup Exclusions
Useful if you want to exclude large directories of non-essential data from your server backups, for example in Minecraft you may want to exclude your dynmap rendered tile data, As it can be huge!
<center>
![Backup Exclusions Example](../../img/page-assets/user-guide/backup-manager/backup-manager-exclusions.png)
<br>Click the "backup exclusions" button to open a menu where you select what paths to exclude in your backup.

<small>Excluded paths will be listed below in the backup menu.</small>
</center>

### Schedule Backups
**Hey!** Remember you can schedule backups with the task scheduler,<br>
For more information you should head over to the [scheduler documentation :material-calendar-end:](task-scheduler.md)

-----
<center>
### Backup Actions

![Backup Actions](../../img/page-assets/user-guide/backup-manager/backup-manager-actions.png)
##### Backup Now
<small>Hit this to create an immediate backup with your current config</small>
##### Download Backup
<small>Hit the blue download button to download a `.zip` archive of your backup content</small>
##### Delete Backup
<small>Hit the red delete button to delete a specific backup</small>
##### Restore Backup
<small>Hit the yellow restore button to restore a backup, you will be prompted to confirm as all current server files will changed to backup state and will be unrecoverable.</small>
##### Saving your backup config
<small>It's something you may miss but after configuring the backup manager, dont forget to hit save!</small>
</center>
